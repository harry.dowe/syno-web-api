<?php

declare(strict_types=1);

use PhpCsFixer\Config;
use PhpCsFixer\Finder;

$finder = (new Finder())
    ->in('src')
    ->in('tests')
    ->exclude('var');

return (new Config())
    ->setRiskyAllowed(true)
    ->setFinder($finder)
    ->setCacheFile('var/cache/.php-cs-fixer.cache')
    ->setRules([
        '@PSR12' => true,
        '@PSR12:risky' => true,
        '@Symfony' => true,
        '@Symfony:risky' => true,
        '@DoctrineAnnotation' => true,

        // @PSR12 rules
        'single_class_element_per_statement' => true,
        'single_import_per_statement' => true,
        'visibility_required' => [
            'elements' => [
                'property',
                'method',
                'const',
            ],
        ],
        'ordered_class_elements' => true,
        'no_break_comment' => false,
        'single_space_around_construct' => true,
        'no_extra_blank_lines' => [
            'tokens' => [
                'continue',
                'break',
                'curly_brace_block',
                'return',
                'throw',
            ],
        ],
        'braces_position' => true,
        'control_structure_continuation_position' => true,
        'control_structure_braces' => true,
        'declare_parentheses' => true,
        'no_multiple_statements_per_line' => true,
        'ordered_imports' => [
            'imports_order' => [
                'class',
                'function',
                'const',
            ],
        ],
        'new_with_parentheses' => true,

        // @Symfony rules
        'array_syntax' => [
            'syntax' => 'short',
        ],
        'blank_line_before_statement' => [
            'statements' => [
                'break',
                'continue',
                'declare',
                'return',
                'throw',
                'try',
                'exit',
                'goto',
            ],
        ],
        'class_attributes_separation' => [
            'elements' => [
                'const' => 'only_if_meta',
                'method' => 'one',
                'property' => 'only_if_meta',
                'trait_import' => 'none',
                'case' => 'none',
            ],
        ],
        'class_definition' => [
            'inline_constructor_arguments' => false,
            'space_before_parenthesis' => true,
            'single_item_single_line' => true,
            'multi_line_extends_each_single_line' => true,
        ],
        'concat_space' => [
            'spacing' => 'one',
        ],
        // Although this matches the @Symfony configuration exactly, this is set to reinforce this configuration is intentional.
        'method_argument_space' => [
            'on_multiline' => 'ignore',
        ],
        'fully_qualified_strict_types' => true,
        'no_superfluous_phpdoc_tags' => [
            'allow_mixed' => true,
        ],
        'no_unneeded_control_parentheses' => [
            'statements' => [
                'break',
                'clone',
                'continue',
                'echo_print',
                'negative_instanceof',
                'others',
                'return',
                'switch_case',
                'yield',
                'yield_from',
            ],
        ],
        'no_unneeded_braces' => [
            'namespaces' => false,
        ],
        'no_unset_cast' => true,
        'no_useless_nullsafe_operator' => true,
        'phpdoc_align' => [
            'align' => 'left',
        ],
        'phpdoc_separation' => [
            'groups' => [
                [
                    'template',
                    'template-extends',
                ],
            ],
        ],
        'single_line_comment_style' => true,
        'space_after_semicolon' => true,
        'trailing_comma_in_multiline' => [
            'elements' => [
                'arrays',
                'arguments',
                'match',
                'parameters',
            ],
            'after_heredoc' => true,
        ],
        'whitespace_after_comma_in_array' => [
            'ensure_single_space' => true,
        ],
        'yoda_style' => [
            'equal' => false,
            'identical' => false,
            'less_and_greater' => false,
        ],

        // Disabling @Symfony rules
        'php_unit_fqcn_annotation' => false,
        'phpdoc_to_comment' => false,
        'single_line_throw' => false,
        'native_function_invocation' => false,

        // @Symfony:risky rules
        'fopen_flags' => true,
        'native_constant_invocation' => [
            'scope' => 'namespaced',
        ],

        // Disabling @Symfony:risky rules
        'error_suppression' => false,
        'no_homoglyph_names' => false,
        'ordered_traits' => false,
        'php_unit_mock_short_will_return' => false,
        'psr_autoloading' => false,

        // @PhpCsFixer rules
        'align_multiline_comment' => [
            'comment_type' => 'all_multiline',
        ],
        'combine_consecutive_issets' => true,
        'combine_consecutive_unsets' => true,
        'explicit_indirect_variable' => true,
        'explicit_string_variable' => true,
        'heredoc_to_nowdoc' => true,
        'method_chaining_indentation' => true,
        'multiline_whitespace_before_semicolons' => true,
        'no_superfluous_elseif' => true,
        'no_useless_else' => true,
        'no_useless_return' => true,
        'phpdoc_no_empty_return' => true,
        'phpdoc_var_annotation_correct_order' => true,
        'return_assignment' => true,
        'multiline_comment_opening_closing' => true,
        'operator_linebreak' => true,

        // @PhpCsFixer:risky rules
        'strict_comparison' => true,
        'strict_param' => true,

        // Custom rules
        'assign_null_coalescing_to_coalesce_equal' => true,
        'global_namespace_import' => [
            'import_classes' => true,
            'import_constants' => true,
            'import_functions' => true,
        ],
        'heredoc_indentation' => true,
        'list_syntax' => [
            'syntax' => 'short',
        ],
        'nullable_type_declaration_for_default_null_value' => true,
        'phpdoc_tag_casing' => [
            'tags' => [
                'inheritdoc',
            ],
        ],
        'phpdoc_line_span' => [
            'property' => 'multi',
            'const' => 'multi',
            'method' => 'multi',
        ],
        'regular_callable_call' => true,
        'self_static_accessor' => true,
        'ternary_to_null_coalescing' => true,
        'octal_notation' => true,
        'declare_strict_types' => true,
        'get_class_to_class_keyword' => true,
        'modernize_strpos' => true,
    ]);
