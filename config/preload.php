<?php

declare(strict_types=1);

if (file_exists(dirname(__DIR__) . '/var/cache/production/HarryDowe_SynoWebApp_KernelProductionContainer.preload.php')) {
    require dirname(__DIR__) . '/var/cache/production/HarryDowe_SynoWebApp_KernelProductionContainer.preload.php';
}
