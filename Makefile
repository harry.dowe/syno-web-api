install: composer-install docker-build migrate

composer-install:
	@docker run --rm -it \
		--volume=$(shell pwd):/app \
	    --volume=$(HOME)/.composer:/tmp \
		--workdir=/app \
		--user=$(shell id -u):$(shell id -g) \
		composer:2.8 \
		composer install

composer-install-prod:
	@docker run --rm -it \
		--volume=$(shell pwd):/app \
	    --volume=$(HOME)/.composer:/tmp \
		--workdir=/app \
		--user=$(shell id -u):$(shell id -g) \
		composer:2.8 \
		composer install --no-dev

composer-validate:
	@docker run --rm -it \
		--volume=$(shell pwd):/app \
		--workdir=/app \
		composer:2.8 \
		composer validate --strict

composer-audit:
	@docker run --rm -it \
		--volume=$(shell pwd):/app \
		--workdir=/app \
		web-api-php:composer \
		composer audit

docker-build:
	@docker compose build php

fmt:
	@docker run --rm -it \
		--volume=$(shell pwd):/app \
		--workdir=/app \
		--user=$(shell id -u):$(shell id -g) \
		php:8.2-cli-alpine3.19 \
		php vendor/bin/php-cs-fixer fix

fmt-check:
	@docker run --rm -it \
		--volume=$(shell pwd):/app \
		--workdir=/app \
		--user=$(shell id -u):$(shell id -g) \
		php:8.2-cli-alpine3.19 \
		php vendor/bin/php-cs-fixer check -vv

psalm:
	@docker run --rm -it \
		--volume=$(shell pwd):/app \
		--workdir=/app \
		--user=$(shell id -u):$(shell id -g) \
		php:8.2.27-cli-alpine3.20 \
		php vendor/bin/psalm

psalm-update-baseline:
	@docker run --rm -it \
		--volume=$(shell pwd):/app \
		--workdir=/app \
		--user=$(shell id -u):$(shell id -g) \
		php:8.2.27-cli-alpine3.20 \
		php vendor/bin/psalm --update-baseline

psalm-set-baseline:
	@docker run --rm -it \
		--volume=$(shell pwd):/app \
		--workdir=/app \
		--user=$(shell id -u):$(shell id -g) \
		php:8.2.27-cli-alpine3.20 \
		php vendor/bin/psalm --set-baseline=baseline.psalm.xml

migrate:
	@docker run --rm -it \
		--volume=$(shell pwd):/app \
		--workdir=/app \
		--network=web-api_default \
		web-api-php:latest \
		php bin/console doctrine:migrations:migrate --no-interaction

test:
	@docker run --rm -it \
		--volume=$(shell pwd):/app \
		--workdir=/app \
		--user=$(shell id -u):$(shell id -g) \
		php:8.2-cli-alpine3.19 \
		php bin/phpunit

coverage:
	@docker run --rm -it \
		--volume=$(shell pwd):/app \
		--workdir=/app \
		--env XDEBUG_MODE=coverage \
		--user=$(shell id -u):$(shell id -g) \
		web-api-php \
		php vendor/bin/phpunit --coverage-html=var/coverage --filter=tests

lint: fmt-check lint-yaml lint-container psalm composer-validate test

lint-yaml:
	@docker run --rm -it \
		--volume=$(shell pwd):/app \
		--workdir=/app \
		php:8.2-cli-alpine3.19 \
		php bin/console lint:yaml config/ docs/ compose.yml --parse-tags

lint-container:
	@docker run --rm -it \
		--volume=$(shell pwd):/app \
		--workdir=/app \
		php:8.2-cli-alpine3.19 \
		php bin/console lint:container

clear-cache:
	@docker run --rm -it \
		--volume=$(shell pwd):/app \
		--workdir=/app \
		--user=$(shell id -u):$(shell id -g) \
		php:8.2-cli-alpine3.19 \
		php bin/console cache:clear

deploy: lint composer-install-prod
	@rsync --delete --recursive --times --verbose --checksum --progress --human-readable \
		bin \
		config \
		migrations \
		public \
	 	src \
	 	vendor \
	 	.env \
	 	composer.json \
	 	composer.lock \
	 	dowe@nas:/volume1/web/web-api \
	 	&& ssh -t dowe@nas "/usr/local/bin/php82 /volume1/web/web-api/bin/console cache:clear" \
	 	; make composer-install
