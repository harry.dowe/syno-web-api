<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Tests\Unit\Bandcamp\Dom;

use DateTimeImmutable;
use Generator;
use GuzzleHttp\Psr7\Uri;
use HarryDowe\SynoWebApp\Bandcamp\Dom\BandcampDomCrawler;
use HarryDowe\SynoWebApp\Bandcamp\Dom\BandcampDomCrawlerException;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\DownloadItem;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\DownloadItemType;
use JsonException;
use Mockery\Adapter\Phpunit\MockeryTestCase;
use PHPUnit\Framework\Attributes\DataProvider;
use Symfony\Component\DomCrawler\Crawler;

use const ENT_QUOTES;
use const JSON_THROW_ON_ERROR;

final class BandcampDomCrawlerTest extends MockeryTestCase
{
    /**
     * @return Generator{0: string, 1: DownloadItem}
     *
     * @throws JsonException
     */
    public static function validHtmlDataProvider(): Generator
    {
        yield [
            self::buildHtml([
                'download_items' => [
                    [
                        'item_id' => 22,
                        'artist' => 'The Artist',
                        'title' => 'The Title',
                        'type' => 'album',
                        'art_id' => 34,
                        'release_date' => '01 Jan 2022 00:00:00 GMT',
                        'downloads' => [
                            'flac' => [
                                'url' => 'https://example.com/download?format=flac',
                            ],
                        ],
                    ],
                ],
            ]),
            new DownloadItem(
                22,
                new Uri('https://example.com/download?format=flac'),
                'The Artist',
                'The Title',
                34,
                DownloadItemType::Album,
                new DateTimeImmutable('2022-01-01 00:00:00'),
            ),
        ];

        // Fallback to digital_items
        yield [
            self::buildHtml([
                'digital_items' => [
                    [
                        'item_id' => 99,
                        'band_name' => 'Some Artist',
                        'title' => 'Some Title',
                        'type' => 'track',
                        'art_id' => 77,
                        'release_date' => '07 Jun 2023 12:55:00 GMT',
                        'downloads' => [
                            'flac' => [
                                'url' => 'https://artist.com/track',
                            ],
                        ],
                    ],
                ],
            ]),
            new DownloadItem(
                99,
                new Uri('https://artist.com/track'),
                'Some Artist',
                'Some Title',
                77,
                DownloadItemType::Track,
                new DateTimeImmutable('2023-06-07 12:55:00'),
            ),
        ];
    }

    #[DataProvider('validHtmlDataProvider')]
    public function testItExtractsDownloadItemFromHtml(string $html, DownloadItem $expectedDownloadItem): void
    {
        $crawler = new BandcampDomCrawler(new Crawler());

        $downloadItems = $crawler->getDownloadItems($html);

        self::assertEquals([$expectedDownloadItem], $downloadItems);
    }

    /**
     * @return array<string, array{0: string, 1: string}>
     */
    public static function invalidHtmlDataProvider(): array
    {
        return [
            'invalid html' => [
                'invalid',
                'Unable to extract data-blob attribute',
            ],
            'missing id attribute' => [
                '<html><body><div data-blob="{&quot;hello&quot;:&quot;there&quot;}"></div></body></html>',
                'Unable to extract data-blob attribute',
            ],
            'missing data-blob attribute' => [
                '<html><body><div id="pagedata"></div></body></html>',
                'Unable to extract data-blob attribute',
            ],
            'invalid json' => [
                '<html><body><div id="pagedata" data-blob="not json"></div></body></html>',
                'Unable to decode JSON string',
            ],
            'missing download item' => [
                '<html><body><div id="pagedata" data-blob="{&quot;hello&quot;:&quot;there&quot;}"></div></body></html>',
                'Unable to extract download items',
            ],
            'invalid download item enum' => [
                '<html><body><div id="pagedata" data-blob="{&quot;download_items&quot;:[{&quot;type&quot;:&quot;invalid&quot;}]}"></div></body></html>',
                'Invalid download item type',
            ],
        ];
    }

    #[DataProvider('invalidHtmlDataProvider')]
    public function testItErrors(string $html, string $expectedExceptionMessage): void
    {
        $crawler = new BandcampDomCrawler(new Crawler());

        $this->expectException(BandcampDomCrawlerException::class);
        $this->expectExceptionMessage($expectedExceptionMessage);

        $crawler->getDownloadItems($html);
    }

    private static function buildHtml(array $blob): string
    {
        $attribute = htmlspecialchars(json_encode($blob, JSON_THROW_ON_ERROR), ENT_QUOTES);

        return "<!DOCTYPE html>\n<html lang=\"en\"><head><title></title></title></head></head><body><div id=\"pagedata\" data-blob=\"{$attribute}\"></div></body></html>";
    }
}
