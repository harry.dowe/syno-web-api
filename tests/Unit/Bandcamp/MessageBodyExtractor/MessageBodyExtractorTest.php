<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Tests\Unit\Bandcamp\MessageBodyExtractor;

use HarryDowe\SynoWebApp\Bandcamp\MessageBodyExtractor\MessageBodyExtractorException;
use HarryDowe\SynoWebApp\Bandcamp\MessageBodyExtractor\MessageBodyUriExtractor;
use Mockery\Adapter\Phpunit\MockeryTestCase;
use PHPUnit\Framework\Attributes\DataProvider;

final class MessageBodyExtractorTest extends MockeryTestCase
{
    /**
     * @return array<int, array{0: string, 1: string}>
     */
    public static function extractorDataProvider(): array
    {
        return [
            [
                'http://bandcamp.com/download?from=receipt&payment_id=45&sig=123abc',
                'http://bandcamp.com/download?from=receipt&payment_id=45&sig=123abc',
            ],
            [
                'http://bandcamp.com/download?from=email&id=34&payment_id=45&sig=123abc',
                'http://bandcamp.com/download?from=email&id=34&payment_id=45&sig=123abc',
            ],
            [
                "This is an email \n\nhttps://bandcamp.com/download?from=receipt&payment_id=999&sig=a1b2c3\nfoo",
                'https://bandcamp.com/download?from=receipt&payment_id=999&sig=a1b2c3',
            ],
        ];
    }

    #[DataProvider('extractorDataProvider')]
    public function testItExtractUriFromEmailBody(string $email, string $expectedUri): void
    {
        $extractor = new MessageBodyUriExtractor();

        $uri = $extractor->extractDownloadPageUri($email);

        self::assertSame($expectedUri, (string) $uri);
    }

    /**
     * @return array<string, array{0: string}>
     */
    public static function exceptionDataProvider(): array
    {
        return [
            'invalid' => ['not valid'],
            'invalid payment_id' => ['http://bandcamp.com/download?from=email&id=34&payment_id=df&sig=123abc'],
            'invalid path' => ['http://bandcamp.com/downloads?from=email&id=34&payment_id=df&sig=123abc'],
            'incorrect params order' => ['http://bandcamp.com/download?from=email&payment_id=45&id=45&sig=123abc'],
        ];
    }

    #[DataProvider('exceptionDataProvider')]
    public function testItThrowsExceptionWhenUriDoesNotMatch(string $email): void
    {
        $extractor = new MessageBodyUriExtractor();

        $this->expectException(MessageBodyExtractorException::class);

        $extractor->extractDownloadPageUri($email);
    }

    public function testItAcceptsHost(): void
    {
        $extractor = new MessageBodyUriExtractor('example.com');

        $uri = $extractor->extractDownloadPageUri('http://example.com/download?from=receipt&payment_id=45&sig=123abc');

        self::assertSame('http://example.com/download?from=receipt&payment_id=45&sig=123abc', (string) $uri);
    }

    public function testItAcceptsHostAndThrowsExceptionWhenUriDoesNotMatch(): void
    {
        $extractor = new MessageBodyUriExtractor('example.com');

        $this->expectException(MessageBodyExtractorException::class);

        $extractor->extractDownloadPageUri('http://bandcamp.com/download?from=receipt&payment_id=45&sig=123abc');
    }
}
