<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Tests\Unit\Bandcamp\Http\Download\Art;

use DateTimeImmutable;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Uri;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\Art\ArtDownloader;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\DownloadItem;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\DownloadItemType;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryTestCase;

final class ArtDownloaderTest extends MockeryTestCase
{
    public function testItDownloadsArt(): void
    {
        $http = Mockery::mock(ClientInterface::class);

        $downloader = new ArtDownloader($http, 'some-location');

        $http->shouldReceive('request')
            ->with('GET', 'img/a34_10.jpg', ['sink' => 'some-location/1.jpg'])
            ->once();

        $downloader->downloadArt(new DownloadItem(
            1,
            new Uri(),
            '',
            '',
            34,
            DownloadItemType::Track,
            new DateTimeImmutable(),
        ));
    }
}
