<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Tests\Unit\Bandcamp\Http\Download\Music;

use DateTimeImmutable;
use Exception;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Uri;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\DownloadItem;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\DownloadItemType;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\Music\MusicDownloader;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\Music\MusicDownloaderException;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryTestCase;

final class MusicDownloaderTest extends MockeryTestCase
{
    public function testItDownloadsFile(): void
    {
        $http = Mockery::mock(ClientInterface::class);

        $downloader = new MusicDownloader($http, 'some-location');

        $http->shouldReceive('request')
            ->with(
                'GET',
                'statdownload/track',
                Mockery::on(static function ($argument): bool {
                    if (!is_array($argument)) {
                        return false;
                    }

                    if (!isset($argument['query']['.rand'])) {
                        return false;
                    }

                    $rand = $argument['query']['.rand'] ?? null;

                    unset($argument['query']['.rand']);

                    return is_numeric($rand) && $argument === [
                        'query' => [
                            'test' => 'hello',
                            '.vrs' => 1,
                        ],
                        'headers' => [
                            'Accept' => 'application/json',
                        ],
                    ];
                }),
            )
            ->once()
            ->andReturn(new Response(body: '{"result":"ok","download_url":"https://example.com/uri/1"}'));

        $http->shouldReceive('request')
            ->with('GET', Mockery::on(static function ($uri): bool {
                return (string) $uri === 'https://example.com/uri/1';
            }), ['sink' => 'some-location/1.flac']);

        $downloader->downloadMusic(new DownloadItem(
            1,
            new Uri('http://example.com/download/track?test=hello'),
            '',
            '',
            34,
            DownloadItemType::Track,
            new DateTimeImmutable(),
        ));
    }

    public function testItThrowsErrorWhenStatDownloadFails(): void
    {
        $http = Mockery::mock(ClientInterface::class);

        $downloader = new MusicDownloader($http, 'some-location');

        $http->shouldReceive('request')
            ->with('GET', 'statdownload/album', Mockery::type('array'))
            ->once()
            ->andThrow(new class () extends Exception implements GuzzleException {});

        $this->expectException(MusicDownloaderException::class);

        $downloader->downloadMusic(new DownloadItem(
            1,
            new Uri('http://example.com/download/album?test=hello'),
            '',
            '',
            34,
            DownloadItemType::Track,
            new DateTimeImmutable(),
        ));
    }

    public function testItThrowsExceptionWhenItHandlesErrorResponse(): void
    {
        $http = Mockery::mock(ClientInterface::class);

        $downloader = new MusicDownloader($http, 'some-location');

        $http->shouldReceive('request')
            ->with('GET', 'statdownload/licensed_album', Mockery::type('array'))
            ->once()
            ->andReturn(new Response(body: '{"result":"err","errortype":"SomeError"}'));

        $this->expectException(MusicDownloaderException::class);

        $downloader->downloadMusic(new DownloadItem(
            1,
            new Uri('http://example.com/download/licensed_album?test=hello'),
            '',
            '',
            34,
            DownloadItemType::Track,
            new DateTimeImmutable(),
        ));
    }

    public function testItThrowsExceptionWhenDownloadFails(): void
    {
        $http = Mockery::mock(ClientInterface::class);

        $downloader = new MusicDownloader($http, 'some-location');

        $http->shouldReceive('request')
            ->with('GET', 'statdownload/album', Mockery::type('array'))
            ->once()
            ->andReturn(new Response(body: '{"result":"ok","download_url":"https://example.com/uri/1"}'));

        $http->shouldReceive('request')
            ->withSomeOfArgs('GET')
            ->once()
            ->andThrow(new class () extends Exception implements GuzzleException {});

        $this->expectException(MusicDownloaderException::class);

        $downloader->downloadMusic(new DownloadItem(
            1,
            new Uri('http://example.com/download/album?test=hello'),
            '',
            '',
            34,
            DownloadItemType::Album,
            new DateTimeImmutable(),
        ));
    }
}
