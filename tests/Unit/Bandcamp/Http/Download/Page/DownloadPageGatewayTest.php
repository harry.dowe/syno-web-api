<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Tests\Unit\Bandcamp\Http\Download\Page;

use Exception;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Uri;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\Page\DownloadPageGateway;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\Page\DownloadPageGatewayException;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryTestCase;
use Psr\Http\Message\UriInterface;

final class DownloadPageGatewayTest extends MockeryTestCase
{
    public function testItDownloadsPage(): void
    {
        $http = Mockery::mock(ClientInterface::class);

        $gateway = new DownloadPageGateway($http);

        $http->shouldReceive('request')
            ->with('GET', Mockery::on(static function (UriInterface $uri): bool {
                self::assertSame('https://example.com/hello?test=1', (string) $uri);

                return true;
            }))
            ->once()
            ->andReturn(new Response(body: 'some body'));

        $body = $gateway->getDownloadPage(new Uri('http://example.com/hello?test=1'));

        self::assertSame('some body', $body);
    }

    public function testItThrowsExceptionWhenDownloadFails(): void
    {
        $http = Mockery::mock(ClientInterface::class);

        $gateway = new DownloadPageGateway($http);

        $http->shouldReceive('request')
            ->with('GET', Mockery::type(UriInterface::class))
            ->once()
            ->andThrow(new class () extends Exception implements GuzzleException {});

        $this->expectException(DownloadPageGatewayException::class);

        $gateway->getDownloadPage(new Uri('http://example.com/hello?test=1'));
    }
}
