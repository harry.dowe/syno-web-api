<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Tests\Unit\Bandcamp;

use Exception;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Uri;
use HarryDowe\SynoWebApp\Bandcamp\Dom\BandcampDomCrawlerInterface;
use HarryDowe\SynoWebApp\Bandcamp\Filesystem\DownloadItemFilesystemInterface;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\Art\ArtDownloaderInterface;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\DownloadItem;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\DownloadItemType;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\Music\MusicDownloaderInterface;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\Page\DownloadPageGatewayInterface;
use HarryDowe\SynoWebApp\Bandcamp\MessageBodyExtractor\MessageBodyExtractorInterface;
use HarryDowe\SynoWebApp\Bandcamp\WebhookBandcampPlexHandler;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryTestCase;
use Mockery\MockInterface;
use Psr\Log\LoggerInterface;

final class WebhookBandcampPlexHandlerTest extends MockeryTestCase
{
    private DownloadPageGatewayInterface&MockInterface $downloadPageGateway;
    private BandcampDomCrawlerInterface&MockInterface $crawler;
    private MessageBodyExtractorInterface&MockInterface $messageBodyExtractor;
    private MusicDownloaderInterface&MockInterface $musicDownloader;
    private ArtDownloaderInterface&MockInterface $artDownloader;
    private DownloadItemFilesystemInterface&MockInterface $filesystem;
    private LoggerInterface&MockInterface $logger;
    private WebhookBandcampPlexHandler $handler;

    public function testItHandlesAlbum(): void
    {
        $uri = new Uri();

        $this->messageBodyExtractor
            ->shouldReceive('extractDownloadPageUri')
            ->with('email')
            ->once()
            ->andReturn($uri);

        $this->downloadPageGateway
            ->shouldReceive('getDownloadPage')
            ->with($uri)
            ->once()
            ->andReturn('html');

        $downloadItem = new DownloadItem(
            1,
            new Uri(),
            '',
            '',
            1,
            DownloadItemType::Album,
            null,
        );

        $this->crawler
            ->shouldReceive('getDownloadItems')
            ->with('html')
            ->once()
            ->andReturn([$downloadItem]);

        $this->musicDownloader
            ->shouldReceive('downloadMusic')
            ->with($downloadItem)
            ->once();

        $this->filesystem
            ->shouldReceive('extractZip')
            ->with($downloadItem)
            ->once();

        $this->handler->processEmail('email');
    }

    public function testItHandlesTrack(): void
    {
        $uri = new Uri();

        $this->messageBodyExtractor
            ->shouldReceive('extractDownloadPageUri')
            ->with('email')
            ->once()
            ->andReturn($uri);

        $this->downloadPageGateway
            ->shouldReceive('getDownloadPage')
            ->with($uri)
            ->once()
            ->andReturn('html');

        $downloadItem = new DownloadItem(
            1,
            new Uri(),
            '',
            '',
            1,
            DownloadItemType::Track,
            null,
        );

        $this->crawler
            ->shouldReceive('getDownloadItems')
            ->with('html')
            ->once()
            ->andReturn([$downloadItem]);

        $this->musicDownloader
            ->shouldReceive('downloadMusic')
            ->with($downloadItem)
            ->once();

        $this->artDownloader
            ->shouldReceive('downloadArt')
            ->with($downloadItem)
            ->once();

        $this->filesystem
            ->shouldReceive('moveTrack')
            ->with($downloadItem)
            ->once();

        $this->handler->processEmail('email');
    }

    public function testItLogsErrorWhenArtDownloadFails(): void
    {
        $uri = new Uri();

        $this->messageBodyExtractor
            ->shouldReceive('extractDownloadPageUri')
            ->with('email')
            ->once()
            ->andReturn($uri);

        $this->downloadPageGateway
            ->shouldReceive('getDownloadPage')
            ->with($uri)
            ->once()
            ->andReturn('html');

        $downloadItem = new DownloadItem(
            1,
            new Uri(),
            '',
            '',
            1,
            DownloadItemType::Track,
            null,
        );

        $this->crawler
            ->shouldReceive('getDownloadItems')
            ->with('html')
            ->once()
            ->andReturn([$downloadItem]);

        $this->musicDownloader
            ->shouldReceive('downloadMusic')
            ->with($downloadItem)
            ->once();

        $this->artDownloader
            ->shouldReceive('downloadArt')
            ->with($downloadItem)
            ->once()
            ->andThrow(new class () extends Exception implements GuzzleException {});

        $this->logger->shouldReceive('error')->once();

        $this->filesystem
            ->shouldReceive('moveTrack')
            ->with($downloadItem)
            ->once();

        $this->handler->processEmail('email');
    }

    protected function mockeryTestSetUp(): void
    {
        $this->downloadPageGateway = Mockery::mock(DownloadPageGatewayInterface::class);
        $this->crawler = Mockery::mock(BandcampDomCrawlerInterface::class);
        $this->messageBodyExtractor = Mockery::mock(MessageBodyExtractorInterface::class);
        $this->musicDownloader = Mockery::mock(MusicDownloaderInterface::class);
        $this->artDownloader = Mockery::mock(ArtDownloaderInterface::class);
        $this->filesystem = Mockery::mock(DownloadItemFilesystemInterface::class);
        $this->logger = Mockery::mock(LoggerInterface::class);

        $this->handler = new WebhookBandcampPlexHandler(
            $this->messageBodyExtractor,
            $this->downloadPageGateway,
            $this->crawler,
            $this->musicDownloader,
            $this->artDownloader,
            $this->filesystem,
            $this->logger,
        );
    }
}
