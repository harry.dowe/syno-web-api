<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Tests\Unit\Bandcamp\Events;

use Exception;
use HarryDowe\SynoWebApp\Bandcamp\Events\BandcampEmailReceivedEvent;
use HarryDowe\SynoWebApp\Bandcamp\Events\BandcampPlexWebhookListener;
use HarryDowe\SynoWebApp\Bandcamp\Events\EmailHandlerCompletedEvent;
use HarryDowe\SynoWebApp\Bandcamp\Events\EmailHandlerFailedEvent;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\DownloadItem;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\DownloadItemType;
use HarryDowe\SynoWebApp\Bandcamp\Repository\BandcampEmailRepositoryInterface;
use HarryDowe\SynoWebApp\Bandcamp\WebhookBandcampPlexHandlerInterface;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryTestCase;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Http\Message\UriInterface;
use Psr\Log\LoggerInterface;

final class BandcampPlexWebhookListenerTest extends MockeryTestCase
{
    public function testHandlesEvent(): void
    {
        $handler = Mockery::mock(WebhookBandcampPlexHandlerInterface::class);
        $db = Mockery::mock(BandcampEmailRepositoryInterface::class);
        $eventDispatcher = Mockery::mock(EventDispatcherInterface::class);
        $logger = Mockery::mock(LoggerInterface::class);

        $listener = new BandcampPlexWebhookListener($handler, $db, $eventDispatcher, $logger);

        $db->shouldReceive('emailExists')->with('123')->once()->andReturn(false);

        $event = new BandcampEmailReceivedEvent('email', '123');

        $db->shouldReceive('createEmail')->with('123', $event->receivedAt)->once();

        $downloadItem = new DownloadItem(1, Mockery::mock(UriInterface::class), '', '', 1, DownloadItemType::Album, null);

        $handler->shouldReceive('processEmail')
            ->with('email')
            ->once()
            ->andReturn([$downloadItem]);

        $eventDispatcher->shouldReceive('dispatch')
            ->with(Mockery::on(static fn ($event): bool => match ($event) {
                !$event instanceof EmailHandlerCompletedEvent,
                $event->messageId !== '123',
                $event->downloadItems === [$downloadItem] => false,
                default => true,
            }))
            ->once();

        $listener->onEmailReceived($event);
    }

    public function testHandlesError(): void
    {
        $handler = Mockery::mock(WebhookBandcampPlexHandlerInterface::class);
        $db = Mockery::mock(BandcampEmailRepositoryInterface::class);
        $eventDispatcher = Mockery::mock(EventDispatcherInterface::class);
        $logger = Mockery::mock(LoggerInterface::class);

        $listener = new BandcampPlexWebhookListener($handler, $db, $eventDispatcher, $logger);

        $db->shouldReceive('emailExists')->with('123')->once()->andReturn(false);

        $event = new BandcampEmailReceivedEvent('email', '123');

        $db->shouldReceive('createEmail')->with('123', $event->receivedAt)->once();

        $handler->shouldReceive('processEmail')
            ->with('email')
            ->andThrow(Exception::class)
            ->once();

        $eventDispatcher->shouldReceive('dispatch')
            ->with(Mockery::on(static fn ($event): bool => match ($event) {
                !$event instanceof EmailHandlerFailedEvent,
                $event->messageId !== '123' => false,
                default => true,
            }))
            ->once();

        $this->expectException(Exception::class);

        $listener->onEmailReceived($event);
    }

    public function testItDoesNotProcessEmailIfExists(): void
    {
        $handler = Mockery::mock(WebhookBandcampPlexHandlerInterface::class);
        $db = Mockery::mock(BandcampEmailRepositoryInterface::class);
        $eventDispatcher = Mockery::mock(EventDispatcherInterface::class);
        $logger = Mockery::mock(LoggerInterface::class);

        $listener = new BandcampPlexWebhookListener($handler, $db, $eventDispatcher, $logger);

        $db->shouldReceive('emailExists')->with('123')->once()->andReturn(true);

        $logger->shouldReceive('warning')->once();

        $listener->onEmailReceived(new BandcampEmailReceivedEvent('email', '123'));
    }
}
