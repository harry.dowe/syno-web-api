<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Tests\Unit\Bandcamp\Events;

use Doctrine\DBAL\Exception as DbalException;
use Exception;
use HarryDowe\SynoWebApp\Bandcamp\Events\EmailHandlerCompletedEvent;
use HarryDowe\SynoWebApp\Bandcamp\Events\EmailHandlerFailedEvent;
use HarryDowe\SynoWebApp\Bandcamp\Events\EmailHandlingCompletedEventListener;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\DownloadItem;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\DownloadItemType;
use HarryDowe\SynoWebApp\Bandcamp\Repository\BandcampEmailRepositoryInterface;
use HarryDowe\SynoWebApp\Bandcamp\Repository\BandcampEmailStatus;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryTestCase;
use Psr\Http\Message\UriInterface;
use Psr\Log\LoggerInterface;

final class EmailHandlingCompletedEventListenerTest extends MockeryTestCase
{
    public function testItHandlesCompletedEvent(): void
    {
        $db = Mockery::mock(BandcampEmailRepositoryInterface::class);
        $logger = Mockery::spy(LoggerInterface::class);

        $handler = new EmailHandlingCompletedEventListener($db, $logger);

        $downloadItem = new DownloadItem(1, Mockery::mock(UriInterface::class), '', '', 1, DownloadItemType::Album, null);

        $event = new EmailHandlerCompletedEvent('123', [$downloadItem]);

        $db->shouldReceive('setEmailStatus')
            ->with(BandcampEmailStatus::Completed, '123', $event->completedAt)
            ->once();

        $handler->onEmailHandlerCompleted($event);
    }

    public function testItCatchesDatabaseErrorWhenHandlingCompletedEvent(): void
    {
        $db = Mockery::mock(BandcampEmailRepositoryInterface::class);
        $logger = Mockery::spy(LoggerInterface::class);

        $handler = new EmailHandlingCompletedEventListener($db, $logger);

        $downloadItem = new DownloadItem(1, Mockery::mock(UriInterface::class), '', '', 1, DownloadItemType::Album, null);

        $event = new EmailHandlerCompletedEvent('123', [$downloadItem]);

        $exception = new class () extends Exception implements DbalException {
        };

        $db->shouldReceive('setEmailStatus')
            ->with(BandcampEmailStatus::Completed, '123', $event->completedAt)
            ->andThrow($exception)
            ->once();

        $handler->onEmailHandlerCompleted($event);

        $logger->shouldHaveReceived('error')->with('Failed to set email as Completed', [
            'messageId' => $event->messageId,
            'exception' => $exception,
        ]);
    }

    public function testItHandlesFailedEvent(): void
    {
        $db = Mockery::mock(BandcampEmailRepositoryInterface::class);
        $logger = Mockery::spy(LoggerInterface::class);

        $handler = new EmailHandlingCompletedEventListener($db, $logger);

        $event = new EmailHandlerFailedEvent('123');

        $db->shouldReceive('setEmailStatus')
            ->with(BandcampEmailStatus::Failed, '123', $event->completedAt)
            ->once();

        $handler->onEmailHandlerFailed($event);
    }

    public function testItCatchesDatabaseErrorWhenHandlingFailedEvent(): void
    {
        $db = Mockery::mock(BandcampEmailRepositoryInterface::class);
        $logger = Mockery::spy(LoggerInterface::class);

        $handler = new EmailHandlingCompletedEventListener($db, $logger);

        $event = new EmailHandlerFailedEvent('123');
        $dbException = new class () extends Exception implements DbalException {
        };

        $db->shouldReceive('setEmailStatus')
            ->with(BandcampEmailStatus::Failed, '123', $event->completedAt)
            ->andThrow($dbException)
            ->once();

        $handler->onEmailHandlerFailed($event);

        $logger->shouldHaveReceived('error')
            ->with('Failed to set email as Failed', [
                'messageId' => $event->messageId,
                'exception' => $dbException,
            ])
            ->once();
    }
}
