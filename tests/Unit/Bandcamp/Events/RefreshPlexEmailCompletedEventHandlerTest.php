<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Tests\Unit\Bandcamp\Events;

use Exception;
use GuzzleHttp\Exception\GuzzleException;
use HarryDowe\SynoWebApp\Bandcamp\Events\RefreshPlexEmailCompletedEventHandler;
use HarryDowe\SynoWebApp\Plex\Http\Client\PlexGatewayInterface;
use HarryDowe\SynoWebApp\Plex\PlexLibraryId;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryTestCase;
use Psr\Log\LoggerInterface;

final class RefreshPlexEmailCompletedEventHandlerTest extends MockeryTestCase
{
    public function testItRefreshesPlexLibrary(): void
    {
        $gateway = Mockery::mock(PlexGatewayInterface::class);
        $logger = Mockery::mock(LoggerInterface::class);

        $handler = new RefreshPlexEmailCompletedEventHandler($gateway, $logger);

        $gateway->shouldReceive('refreshLibrary')
            ->with(PlexLibraryId::Music)
            ->once();

        $logger->shouldReceive('info')->once();

        $handler->onEmailHandlerCompleted();
    }

    public function testItDoesNotRefreshLibrary(): void
    {
        $gateway = Mockery::mock(PlexGatewayInterface::class);
        $logger = Mockery::mock(LoggerInterface::class);

        $handler = new RefreshPlexEmailCompletedEventHandler($gateway, $logger);

        $gateway->shouldReceive('refreshLibrary')
            ->with(PlexLibraryId::Music)
            ->once()
            ->andThrow(new class () extends Exception implements GuzzleException {});

        $logger->shouldReceive('error')->once();

        $handler->onEmailHandlerCompleted();
    }
}
