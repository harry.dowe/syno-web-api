<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Tests\Unit\Bandcamp\Events;

use HarryDowe\SynoWebApp\Bandcamp\Events\EmailHandlerCompletedEvent;
use HarryDowe\SynoWebApp\Bandcamp\Events\FilesystemCleanupEmailCompletedEventListener;
use HarryDowe\SynoWebApp\Bandcamp\Filesystem\DownloadItemFilesystemInterface;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\DownloadItem;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\DownloadItemType;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryTestCase;
use Psr\Http\Message\UriInterface;
use Psr\Log\LoggerInterface;

final class FilesystemCleanupEmailCompletedEventListenerTest extends MockeryTestCase
{
    public function testItDoesNotHandleTracks(): void
    {
        $filesystem = Mockery::mock(DownloadItemFilesystemInterface::class);

        $handler = new FilesystemCleanupEmailCompletedEventListener(
            $filesystem,
            Mockery::mock(LoggerInterface::class),
        );

        $filesystem->shouldNotReceive('extractZip');

        $handler->onEmailCompleted(new EmailHandlerCompletedEvent('1', [
            new DownloadItem(
                1,
                Mockery::mock(UriInterface::class),
                '',
                '',
                1,
                DownloadItemType::Track,
                null,
            ),
        ]));
    }

    public function testItDeletesZip(): void
    {
        $filesystem = Mockery::mock(DownloadItemFilesystemInterface::class);

        $handler = new FilesystemCleanupEmailCompletedEventListener(
            $filesystem,
            Mockery::mock(LoggerInterface::class),
        );

        $downloadItem = new DownloadItem(
            1,
            Mockery::mock(UriInterface::class),
            '',
            '',
            1,
            DownloadItemType::Album,
            null,
        );

        $filesystem->shouldReceive('deleteZip')
            ->with($downloadItem)
            ->once();

        $handler->onEmailCompleted(new EmailHandlerCompletedEvent('1', [$downloadItem]));
    }
}
