<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Tests\Unit\Bandcamp\Filesystem;

use DateTimeImmutable;
use GuzzleHttp\Psr7\Uri;
use HarryDowe\SynoWebApp\Bandcamp\Filesystem\DownloadItemFilesystem;
use HarryDowe\SynoWebApp\Bandcamp\Filesystem\DownloadItemFilesystemAdapterInterface;
use HarryDowe\SynoWebApp\Bandcamp\Filesystem\FilesystemException;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\DownloadItem;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\DownloadItemType;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryTestCase;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

final class DownloadItemFilesystemTest extends MockeryTestCase
{
    public function testItExtractsZipFile(): void
    {
        $adapter = Mockery::mock(DownloadItemFilesystemAdapterInterface::class);
        $filesystem = new DownloadItemFilesystem($adapter, new NullLogger());

        $downloadItem = new DownloadItem(
            1,
            new Uri(),
            'artist',
            'title',
            2,
            DownloadItemType::Album,
            new DateTimeImmutable('2022-02-03 12:50:00'),
        );

        $adapter->shouldReceive('extract')
            ->with('1.zip', 'artist/(2022) title')
            ->once();

        $filesystem->extractZip($downloadItem);
    }

    public function testItDeletesZipFile(): void
    {
        $adapter = Mockery::mock(DownloadItemFilesystemAdapterInterface::class);
        $filesystem = new DownloadItemFilesystem($adapter, new NullLogger());

        $downloadItem = new DownloadItem(
            1,
            new Uri(),
            'artist',
            'title',
            2,
            DownloadItemType::Album,
            new DateTimeImmutable('2022-02-03 12:50:00'),
        );

        $adapter->shouldReceive('delete')
            ->with('1.zip')
            ->once();

        $filesystem->deleteZip($downloadItem);
    }

    public function testItMovesCoverArtAndTrack(): void
    {
        $adapter = Mockery::mock(DownloadItemFilesystemAdapterInterface::class);
        $filesystem = new DownloadItemFilesystem($adapter, Mockery::mock(LoggerInterface::class));

        $downloadItem = new DownloadItem(
            1,
            new Uri(),
            'artist',
            'title',
            2,
            DownloadItemType::Track,
            new DateTimeImmutable('2022-02-03 12:50:00'),
        );

        $adapter->shouldReceive('move')
            ->with('1.jpg', 'artist/(2022) title/cover.jpg')
            ->once();

        $adapter->shouldReceive('move')
            ->with('1.flac', 'artist/(2022) title/artist - title.flac')
            ->once();

        $filesystem->moveTrack($downloadItem);
    }

    public function testItMovesTrackWithoutReleaseDate(): void
    {
        $adapter = Mockery::mock(DownloadItemFilesystemAdapterInterface::class);
        $logger = Mockery::mock(LoggerInterface::class);
        $filesystem = new DownloadItemFilesystem($adapter, $logger);

        $downloadItem = new DownloadItem(
            1,
            new Uri(),
            'artist',
            'title',
            2,
            DownloadItemType::Track,
            null,
        );

        $adapter->shouldReceive('move')
            ->with('1.jpg', 'artist/title/cover.jpg')
            ->andThrow(FilesystemException::class);

        $adapter->shouldReceive('move')
            ->with('1.flac', 'artist/title/artist - title.flac')
            ->once();

        $logger->shouldReceive('error')->once();

        $filesystem->moveTrack($downloadItem);
    }
}
