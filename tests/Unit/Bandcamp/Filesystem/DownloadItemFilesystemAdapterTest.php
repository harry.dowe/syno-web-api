<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Tests\Unit\Bandcamp\Filesystem;

use Exception;
use HarryDowe\SynoWebApp\Bandcamp\Filesystem\DownloadItemFilesystemAdapter;
use HarryDowe\SynoWebApp\Bandcamp\Filesystem\FilesystemException;
use League\Flysystem\FilesystemException as FlysystemException;
use League\Flysystem\FilesystemWriter;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryTestCase;
use PhpZip\Exception\ZipException;
use PhpZip\ZipFile;

final class DownloadItemFilesystemAdapterTest extends MockeryTestCase
{
    public function testItExtractsZip(): void
    {
        $zip = Mockery::mock(ZipFile::class);
        $filesystemWriter = Mockery::mock(FilesystemWriter::class);

        $adapter = new DownloadItemFilesystemAdapter(
            $zip,
            'symfony/bandcamp',
            'destination/plex',
            $filesystemWriter,
        );

        $zip->shouldReceive('openFile')
            ->with('symfony/bandcamp/file.zip')
            ->once()
            ->andReturnSelf();

        $filesystemWriter->shouldReceive('createDirectory')
            ->with('destination/plex/artist/folder/');

        $zip->shouldReceive('extractTo')
            ->with('destination/plex/artist/folder/')
            ->once();

        $zip->shouldReceive('close');

        $adapter->extract('file.zip', 'artist/folder');
    }

    public function testItThrowsExceptionWhenDirectoryCannotBeCreated(): void
    {
        $zip = Mockery::mock(ZipFile::class);
        $filesystemWriter = Mockery::mock(FilesystemWriter::class);

        $adapter = new DownloadItemFilesystemAdapter(
            $zip,
            'symfony/bandcamp',
            'destination/plex',
            $filesystemWriter,
        );

        $filesystemWriter->shouldReceive('createDirectory')
            ->with('destination/plex/artist/folder/')
            ->andThrow(new class () extends Exception implements FlysystemException {})
            ->once();

        $this->expectException(FilesystemException::class);
        $adapter->extract('file.zip', 'artist/folder');
    }

    public function testItThrowsExceptionWhenZipCannotBeOpened(): void
    {
        $zip = Mockery::mock(ZipFile::class);
        $filesystemWriter = Mockery::mock(FilesystemWriter::class);

        $adapter = new DownloadItemFilesystemAdapter(
            $zip,
            'symfony/bandcamp',
            'destination/plex',
            $filesystemWriter,
        );

        $filesystemWriter->shouldReceive('createDirectory')
            ->with('destination/plex/artist/folder/')
            ->once();

        $zip->shouldReceive('openFile')
            ->with('symfony/bandcamp/file.zip')
            ->andThrow(ZipException::class)
            ->once();

        $zip->shouldReceive('close')->once();

        $this->expectException(FilesystemException::class);

        $adapter->extract('file.zip', 'artist/folder');
    }

    public function testItThrowsExceptionWhenZipCannotBeExtracted(): void
    {
        $zip = Mockery::mock(ZipFile::class);
        $filesystemWriter = Mockery::mock(FilesystemWriter::class);

        $adapter = new DownloadItemFilesystemAdapter(
            $zip,
            'symfony/bandcamp',
            'destination/plex',
            $filesystemWriter,
        );

        $filesystemWriter->shouldReceive('createDirectory')
            ->with('destination/plex/artist/folder/');

        $zip->shouldReceive('openFile')
            ->with('symfony/bandcamp/file.zip')
            ->once()
            ->andReturnSelf();

        $zip->shouldReceive('extractTo')
            ->with('destination/plex/artist/folder/')
            ->andThrow(ZipException::class)
            ->once();

        $zip->shouldReceive('close');

        $this->expectException(FilesystemException::class);

        $adapter->extract('file.zip', 'artist/folder');
    }

    public function testItMovesFile(): void
    {
        $filesystemWriter = Mockery::mock(FilesystemWriter::class);

        $adapter = new DownloadItemFilesystemAdapter(
            Mockery::mock(ZipFile::class),
            'symfony/bandcamp',
            'destination/plex',
            $filesystemWriter,
        );

        $filesystemWriter->shouldReceive('move')
            ->with('symfony/bandcamp/track.flac', 'destination/plex/artist/track.flac')
            ->once();

        $adapter->move('track.flac', 'artist/track.flac');
    }

    public function testItThrowsExceptionWhenMovingErrors(): void
    {
        $filesystemWriter = Mockery::mock(FilesystemWriter::class);

        $adapter = new DownloadItemFilesystemAdapter(
            Mockery::mock(ZipFile::class),
            'symfony/bandcamp',
            'destination/plex',
            $filesystemWriter,
        );

        $filesystemWriter->shouldReceive('move')
            ->with('symfony/bandcamp/track.flac', 'destination/plex/artist/track.flac')
            ->andThrow(new class () extends Exception implements FlysystemException {})
            ->once();

        $this->expectException(FilesystemException::class);

        $adapter->move('track.flac', 'artist/track.flac');
    }
}
