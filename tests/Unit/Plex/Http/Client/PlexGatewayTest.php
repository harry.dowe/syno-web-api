<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Tests\Unit\Plex\Http\Client;

use GuzzleHttp\ClientInterface;
use HarryDowe\SynoWebApp\Plex\Http\Client\PlexGateway;
use HarryDowe\SynoWebApp\Plex\PlexLibraryId;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryTestCase;
use PHPUnit\Framework\Attributes\DataProvider;

final class PlexGatewayTest extends MockeryTestCase
{
    public static function libraryIdDataProvider(): array
    {
        return array_map(static fn (PlexLibraryId $id): array => [$id], PlexLibraryId::cases());
    }

    #[DataProvider('libraryIdDataProvider')]
    public function testItRefreshesPlexMediaLibrary(PlexLibraryId $id): void
    {
        $client = Mockery::mock(ClientInterface::class);

        $gateway = new PlexGateway($client);

        $client->shouldReceive('request')
            ->with('GET', "library/sections/{$id->value}/refresh")
            ->once();

        $gateway->refreshLibrary($id);
    }
}
