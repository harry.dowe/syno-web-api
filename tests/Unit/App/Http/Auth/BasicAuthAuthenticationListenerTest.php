<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Tests\Unit\App\Http\Auth;

use HarryDowe\SynoWebApp\App\Http\Auth\Basic\BasicAuthAuthenticationListener;
use HarryDowe\SynoWebApp\App\Http\Auth\Basic\BasicAuthenticatedController;
use HarryDowe\SynoWebApp\App\Http\Auth\Basic\WithBasicAuthentication;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryTestCase;
use PHPUnit\Framework\Attributes\DataProvider;
use Symfony\Component\DependencyInjection\Exception\ParameterNotFoundException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use UnexpectedValueException;

final class BasicAuthAuthenticationListenerTest extends MockeryTestCase
{
    /**
     * @return array<string, array{0: Request}>
     */
    public static function unauthenticatedRequestDataProvider(): array
    {
        return [
            'missing auth header' => [new Request()],
            'not base64 encoded' => [new Request(server: ['HTTP_AUTHORIZATION' => 'Basic username:password'])],

            // invalid_user:password
            'invalid username' => [new Request(server: ['HTTP_AUTHORIZATION' => 'Basic aW52YWxpZF91c2VyOnBhc3N3b3Jk'])],

            // username:invalid_password
            'invalid password' => [new Request(server: ['HTTP_AUTHORIZATION' => 'Basic dXNlcm5hbWU6aW52YWxpZF9wYXNzd29yZA=='])],

            // username:
            'missing password' => [new Request(server: ['HTTP_AUTHORIZATION' => 'Basic dXNlcm5hbWU6'])],

            // username
            'missing colon' => [new Request(server: ['HTTP_AUTHORIZATION' => 'Basic dXNlcm5hbWU='])],

            'missing credentials' => [new Request(server: ['HTTP_AUTHORIZATION' => 'Basic '])],
        ];
    }

    public function testItAuthenticatesUsingAttribute(): void
    {
        $parameterBag = new ParameterBag([
            'user_param' => 'username',
            'pass_param' => 'password123',
        ]);

        $listener = new BasicAuthAuthenticationListener(
            $parameterBag,
        );

        $controller = new class () implements BasicAuthenticatedController {
            #[WithBasicAuthentication('user_param', 'pass_param')]
            public function controllerAction()
            {
            }
        };

        $event = new ControllerEvent(
            Mockery::mock(HttpKernelInterface::class),
            [$controller, 'controllerAction'],
            // username:password123
            new Request(server: ['HTTP_AUTHORIZATION' => 'Basic dXNlcm5hbWU6cGFzc3dvcmQxMjM=']),
            null,
        );

        $this->addToAssertionCount(1);

        $listener->onKernelController($event);
    }

    #[DataProvider('unauthenticatedRequestDataProvider')]
    public function testThrowsUnauthenticatedExceptionWhenAuthHeaderIsMissing(Request $request): void
    {
        $parameterBag = new ParameterBag([
            'user_param' => 'username',
            'pass_param' => 'password',
        ]);

        $listener = new BasicAuthAuthenticationListener($parameterBag);

        $controller = new class () implements BasicAuthenticatedController {
            #[WithBasicAuthentication('user_param', 'pass_param')]
            public function controllerAction()
            {
            }
        };

        $event = new ControllerEvent(
            Mockery::mock(HttpKernelInterface::class),
            [$controller, 'controllerAction'],
            $request,
            null,
        );

        $this->expectException(UnauthorizedHttpException::class);

        $listener->onKernelController($event);
    }

    public function testItDoesNothingIfControllerDoesNotImplementInterface(): void
    {
        $listener = new BasicAuthAuthenticationListener(Mockery::mock(ParameterBagInterface::class));

        $event = new ControllerEvent(
            Mockery::mock(HttpKernelInterface::class),
            static fn (): bool => true,
            Mockery::mock(Request::class),
            null,
        );

        $this->addToAssertionCount(1);

        $listener->onKernelController($event);
    }

    public function testItErrorsWhenAttributeIsMissing(): void
    {
        $listener = new BasicAuthAuthenticationListener(Mockery::mock(ParameterBagInterface::class));

        $controller = new class () implements BasicAuthenticatedController {
            public function controllerAction()
            {
            }
        };

        $event = new ControllerEvent(
            Mockery::mock(HttpKernelInterface::class),
            [$controller, 'controllerAction'],
            new Request(server: ['HTTP_AUTHORIZATION' => 'Basic dXNlcm5hbWU6cGFzc3dvcmQxMjM=']),
            null,
        );

        $this->expectException(UnexpectedValueException::class);

        $listener->onKernelController($event);
    }

    public function testItAcceptsInvokable(): void
    {
        $parameterBag = new ParameterBag([
            'user_param' => 'username',
            'pass_param' => 'password123',
        ]);

        $listener = new BasicAuthAuthenticationListener($parameterBag);

        $controller = new class () implements BasicAuthenticatedController {
            #[WithBasicAuthentication('user_param', 'pass_param')]
            public function __invoke()
            {
            }
        };

        $event = new ControllerEvent(
            Mockery::mock(HttpKernelInterface::class),
            $controller,
            new Request(server: ['HTTP_AUTHORIZATION' => 'Basic dXNlcm5hbWU6cGFzc3dvcmQxMjM=']),
            null,
        );

        $this->addToAssertionCount(1);

        $listener->onKernelController($event);
    }

    public function testInvalidFormat(): void
    {
        $parameterBag = new ParameterBag([
            'user_param' => 'username',
            'pass_param' => 'password123',
        ]);

        $listener = new BasicAuthAuthenticationListener($parameterBag);

        $controller = new class () implements BasicAuthenticatedController {
            #[WithBasicAuthentication('user_param', 'pass_param')]
            public function __invoke()
            {
            }
        };

        $event = new ControllerEvent(
            Mockery::mock(HttpKernelInterface::class),
            $controller,
            new Request(server: ['HTTP_AUTHORIZATION' => 'Basic ']),
            null,
        );

        $this->expectException(UnauthorizedHttpException::class);

        $listener->onKernelController($event);
    }

    public function testItThrowsParameterNotFoundExceptionWhenUserParamNotFound(): void
    {
        $parameterBag = new ParameterBag([
            'user_param' => 'username',
            'pass_param' => 'password123',
        ]);

        $listener = new BasicAuthAuthenticationListener($parameterBag);

        $controller = new class () implements BasicAuthenticatedController {
            #[WithBasicAuthentication('user_param_not_found', 'pass_param')]
            public function __invoke()
            {
            }
        };

        $event = new ControllerEvent(
            Mockery::mock(HttpKernelInterface::class),
            $controller,
            new Request(server: ['HTTP_AUTHORIZATION' => 'Basic ']),
            null,
        );

        $this->expectException(ParameterNotFoundException::class);

        $listener->onKernelController($event);
    }

    public function testItThrowsParameterNotFoundExceptionWhenPasswordParamNotFound(): void
    {
        $parameterBag = new ParameterBag([
            'user_param' => 'username',
            'pass_param' => 'password123',
        ]);

        $listener = new BasicAuthAuthenticationListener($parameterBag);

        $controller = new class () implements BasicAuthenticatedController {
            #[WithBasicAuthentication('user_param', 'pass_param_not_found')]
            public function __invoke()
            {
            }
        };

        $event = new ControllerEvent(
            Mockery::mock(HttpKernelInterface::class),
            $controller,
            new Request(server: ['HTTP_AUTHORIZATION' => 'Basic ']),
            null,
        );

        $this->expectException(ParameterNotFoundException::class);

        $listener->onKernelController($event);
    }
}
