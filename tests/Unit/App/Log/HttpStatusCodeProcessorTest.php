<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Tests\Unit\App\Log;

use DateTimeImmutable;
use Exception;
use Generator;
use HarryDowe\SynoWebApp\App\Log\HttpStatusCodeProcessor;
use Mockery\Adapter\Phpunit\MockeryTestCase;
use Monolog\Level;
use Monolog\LogRecord;
use PHPUnit\Framework\Attributes\DataProvider;
use Symfony\Component\HttpKernel\Exception\HttpException;

final class HttpStatusCodeProcessorTest extends MockeryTestCase
{
    /**
     * @return Generator<mixed, array{Level, Level}>
     */
    public static function thresholdDataProvider(): Generator
    {
        yield [Level::Alert, Level::Alert];
        yield [Level::Emergency, Level::Alert];
        yield [Level::Emergency, Level::Debug];
        yield [Level::Info, Level::Debug];
        yield [Level::Notice, Level::Info];
        yield [Level::Error, Level::Warning];
        yield [Level::Critical, Level::Error];
        yield [Level::Alert, Level::Critical];
        yield [Level::Emergency, Level::Alert];
    }

    public function testProcessor(): void
    {
        $processor = new HttpStatusCodeProcessor([404]);

        $result = $processor($record = self::createRecord(Level::Error, [
            'exception' => HttpException::fromStatusCode(404),
        ]));

        self::assertEquals($record->with(level: Level::Debug), $result);
    }

    public function testProcessorMapsToGivenLevel(): void
    {
        $processor = new HttpStatusCodeProcessor([404], Level::Info);

        $result = $processor($record = self::createRecord(Level::Error, [
            'exception' => HttpException::fromStatusCode(404),
        ]));

        self::assertEquals($record->with(level: Level::Info), $result);
    }

    #[DataProvider('thresholdDataProvider')]
    public function testIgnoresAtOrAboveThreshold(Level $level, Level $threshold): void
    {
        $processor = new HttpStatusCodeProcessor([404], Level::Debug, $threshold);

        $result = $processor($record = self::createRecord($level, [
            'exception' => HttpException::fromStatusCode(404),
        ]));

        self::assertSame($record, $result);
    }

    /**
     * @return Generator<mixed, array{int[], array}>
     */
    public static function withoutMutationDataProvider(): Generator
    {
        yield [
            [404], self::createRecord(),
        ];

        yield [
            [404], self::createRecord(Level::Emergency, ['exception' => new Exception()]),
        ];

        yield [
            [404, 403, 401], self::createRecord(Level::Error, ['exception' => new HttpException(400)]),
        ];

        yield [
            [], self::createRecord(Level::Alert, ['exception' => new HttpException(404)]),
        ];
    }

    /**
     * @param int[] $httpCodes
     */
    #[DataProvider('withoutMutationDataProvider')]
    public function testProcessorDoesNotMutate(array $httpCodes, LogRecord $record): void
    {
        $processor = new HttpStatusCodeProcessor($httpCodes);

        $result = $processor($record);

        self::assertSame($record, $result);
    }

    private static function createRecord(Level $level = Level::Error, array $context = []): LogRecord
    {
        return new LogRecord(
            new DateTimeImmutable(),
            '',
            $level,
            '',
            $context,
        );
    }
}
