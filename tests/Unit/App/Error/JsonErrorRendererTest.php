<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Tests\Unit\App\Error;

use Exception;
use Generator;
use HarryDowe\SynoWebApp\App\Error\JsonErrorRenderer;
use Mockery\Adapter\Phpunit\MockeryTestCase;
use PHPUnit\Framework\Attributes\DataProvider;
use RuntimeException;
use Symfony\Component\HttpFoundation\Exception\RequestExceptionInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Throwable;

use const JSON_THROW_ON_ERROR;

final class JsonErrorRendererTest extends MockeryTestCase
{
    /**
     * @return Generator<mixed, array{bool, callable(): bool}
     */
    public static function withExceptionDetailsDataProvider(): Generator
    {
        yield [true];
        yield [static function () { return true; }];
    }

    /**
     * @param bool|callable(): bool $debug
     */
    #[DataProvider('withExceptionDetailsDataProvider')]
    public function testRendersToJsonWithExceptionDetails(
        bool|callable $debug,
    ): void {
        $renderer = new JsonErrorRenderer($debug);

        $result = $renderer->render(new RuntimeException('hello'));

        $exceptionString = $result->getAsString();

        self::assertJson($exceptionString);

        $decoded = json_decode($exceptionString, true, flags: JSON_THROW_ON_ERROR);

        // Assert structure
        self::assertArrayHasKey('error', $decoded);
        self::assertArrayHasKey('exception', $decoded);
        self::assertArrayHasKey('class', $decoded['exception']);
        self::assertArrayHasKey('message', $decoded['exception']);
        self::assertArrayHasKey('code', $decoded['exception']);
        self::assertArrayHasKey('file', $decoded['exception']);
        self::assertArrayHasKey('trace', $decoded['exception']);

        // Assert values
        self::assertSame('hello', $decoded['error']);
        self::assertSame('RuntimeException', $decoded['exception']['class']);
        self::assertSame('hello', $decoded['exception']['message']);
        self::assertSame(0, $decoded['exception']['code']);
        self::assertIsString($decoded['exception']['file']);
        self::assertStringMatchesFormat('%s:%d', $decoded['exception']['file']);
        self::assertIsList($decoded['exception']['trace']);

        foreach ($decoded['exception']['trace'] as $trace) {
            self::assertIsString($trace);
            self::assertStringMatchesFormat('%s:%d', $trace);
        }
    }

    /**
     * @param bool|callable(): bool $debug
     */
    #[DataProvider('withExceptionDetailsDataProvider')]
    public function testHeadersWithDebugDetails(
        bool|callable $debug,
    ): void {
        $renderer = new JsonErrorRenderer($debug);

        $result = $renderer->render(new RuntimeException('hello'));

        $headers = $result->getHeaders();

        self::assertCount(3, $headers);
        self::assertArrayHasKey('Content-Type', $headers);
        self::assertArrayHasKey('X-Debug-Exception', $headers);
        self::assertArrayHasKey('X-Debug-Exception-File', $headers);
        self::assertIsString($headers['X-Debug-Exception-File']);

        self::assertSame('application/json', $headers['Content-Type']);
        self::assertSame('hello', $headers['X-Debug-Exception']);
        self::assertStringMatchesFormat('%s%d', $headers['X-Debug-Exception-File']);
    }

    public static function statusCodeDataProvider(): Generator
    {
        yield [500, new RuntimeException('hello')];
        yield [403, new AccessDeniedHttpException()];
        yield [522, new class () extends Exception implements HttpExceptionInterface {
            public function getStatusCode(): int
            {
                return 522;
            }

            public function getHeaders(): array
            {
                return [];
            }
        }];
        yield [400, new class () extends Exception implements RequestExceptionInterface {}];
    }

    #[DataProvider('statusCodeDataProvider')]
    public function testSetsStatusCode(int $expectedStatusCode, Throwable $exception): void
    {
        $renderer = new JsonErrorRenderer();

        $result = $renderer->render($exception);

        self::assertSame($expectedStatusCode, $result->getStatusCode());
    }

    /**
     * @param bool|callable(): bool $debug
     */
    #[DataProvider('withExceptionDetailsDataProvider')]
    public function testRendersToJsonWithoutExceptionDetails(
        bool|callable $debug,
    ): void {
        $renderer = new JsonErrorRenderer($this->flipDebug($debug));

        $result = $renderer->render(new RuntimeException('hello'));

        self::assertSame(['Content-Type' => 'application/json'], $result->getHeaders());

        $exceptionString = $result->getAsString();

        self::assertJson($exceptionString);

        $decoded = json_decode($exceptionString, true, flags: JSON_THROW_ON_ERROR);

        self::assertSame(['error' => 'hello'], $decoded);
    }

    /**
     * @param bool|callable():bool $debug
     *
     * @return bool|callable():bool
     */
    private function flipDebug(callable|bool $debug): bool|callable
    {
        if (is_bool($debug)) {
            return !$debug;
        }

        return static function () use ($debug): bool {
            return !$debug();
        };
    }
}
