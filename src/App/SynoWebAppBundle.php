<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\App;

use HarryDowe\SynoWebApp\App\Log\LoggerConfiguratorCompilePass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;

final class SynoWebAppBundle extends AbstractBundle
{
    public function build(ContainerBuilder $container): void
    {
        $container->addCompilerPass(new LoggerConfiguratorCompilePass());
    }
}
