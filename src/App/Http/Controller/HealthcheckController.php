<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\App\Http\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

final class HealthcheckController extends AbstractController
{
    #[Route(path: '/', methods: [Request::METHOD_GET])]
    #[Route(path: '/healthcheck', methods: [Request::METHOD_GET])]
    public function healthcheck(): Response
    {
        return new JsonResponse([
            'health' => 'ok',
        ]);
    }
}
