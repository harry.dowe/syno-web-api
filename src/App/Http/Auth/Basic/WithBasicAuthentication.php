<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\App\Http\Auth\Basic;

use Attribute;
use Symfony\Component\DependencyInjection\Attribute\Exclude;

#[Exclude]
#[Attribute(Attribute::TARGET_METHOD)]
final readonly class WithBasicAuthentication
{
    public function __construct(
        public string $usernameParameter,
        public string $passwordParameter,
    ) {
    }
}
