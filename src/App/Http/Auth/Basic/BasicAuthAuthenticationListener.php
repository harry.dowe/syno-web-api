<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\App\Http\Auth\Basic;

use ReflectionClass;
use Symfony\Component\DependencyInjection\Exception\ParameterNotFoundException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use UnexpectedValueException;

final readonly class BasicAuthAuthenticationListener
{
    public function __construct(
        private ParameterBagInterface $parameterBag,
    ) {
    }

    /**
     * @throws UnauthorizedHttpException
     */
    #[AsEventListener(KernelEvents::CONTROLLER)]
    public function onKernelController(ControllerEvent $event): void
    {
        if (!$this->getController($event) instanceof BasicAuthenticatedController) {
            return;
        }

        if (!$event->getRequest()->headers->has('Authorization')) {
            throw new UnauthorizedHttpException('Authorization', 'Missing authorization header');
        }

        [$configuredUser, $configuredPassword] = $this->getCredentialsFromAttribute($event);

        $expectedAuth = base64_encode("{$configuredUser}:{$configuredPassword}");
        $actualAuth = substr($event->getRequest()->headers->get('Authorization', ''), 6, strlen($expectedAuth) + 6);

        if (!hash_equals($expectedAuth, $actualAuth)) {
            throw new UnauthorizedHttpException('Authorization', 'Invalid authorization header');
        }
    }

    private function getController(ControllerEvent $event): string|object
    {
        $controller = $event->getController();

        return match (true) {
            is_array($controller) => $controller[0],
            default => $controller,
        };
    }

    /**
     * @return array{string, string}
     *
     * @throws ParameterNotFoundException
     */
    private function getCredentialsFromAttribute(ControllerEvent $event): array
    {
        [$controller, $method] = $this->getControllerMethod($event);

        $attribute = $this->getAttribute($controller, $method);

        if ($attribute === null) {
            throw new UnexpectedValueException(
                'Controller action ' . $controller::class . "::{$method}() must use attribute " . WithBasicAuthentication::class,
            );
        }

        return [
            $this->parameterBag->get($attribute->usernameParameter),
            $this->parameterBag->get($attribute->passwordParameter),
        ];
    }

    /**
     * @return array{BasicAuthenticatedController, string}
     */
    private function getControllerMethod(ControllerEvent $event): array
    {
        /** @var array{BasicAuthenticatedController, string}|object $controller */
        $controller = $event->getController();

        return match (true) {
            is_array($controller) => $controller,
            default => [$controller, '__invoke'],
        };
    }

    private function getAttribute(BasicAuthenticatedController $controller, string $method): ?WithBasicAuthentication
    {
        $attribute = (new ReflectionClass($controller))
            ->getMethod($method)
            ->getAttributes(WithBasicAuthentication::class)[0] ?? null;

        return $attribute?->newInstance();
    }
}
