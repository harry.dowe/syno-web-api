<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\App\Error;

use Closure;
use JsonException;
use Symfony\Component\ErrorHandler\ErrorRenderer\ErrorRendererInterface;
use Symfony\Component\ErrorHandler\Exception\FlattenException;
use Throwable;

use const JSON_THROW_ON_ERROR;
use const JSON_UNESCAPED_SLASHES;
use const JSON_UNESCAPED_UNICODE;

final readonly class JsonErrorRenderer implements ErrorRendererInterface
{
    private bool|Closure $debug;

    public function __construct(
        bool|callable $debug = false,
    ) {
        $this->debug = is_bool($debug) ? $debug : $debug(...);
    }

    public function render(Throwable $exception): FlattenException
    {
        $error = ['error' => $exception->getMessage()];

        $headers = ['Content-Type' => 'application/json'];

        if (is_bool($this->debug) ? $this->debug : ($this->debug)()) {
            $error['exception'] = $this->formatExceptionFragment($exception);

            $headers['X-Debug-Exception'] = rawurlencode(substr($exception->getMessage(), 0, 2000));
            $headers['X-Debug-Exception-File'] = rawurlencode($exception->getFile()) . $exception->getLine();
        }

        return FlattenException::createFromThrowable($exception, null, $headers)
            ->setAsString($this->encodeException($error));
    }

    private function encodeException(array $error): ?string
    {
        try {
            return json_encode($error, JSON_THROW_ON_ERROR | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        } catch (JsonException) {
            return null;
        }
    }

    private function formatExceptionFragment(Throwable $exception): array
    {
        $data = [
            'class' => $exception::class,
            'message' => $exception->getMessage(),
            'code' => $exception->getCode(),
            'file' => "{$exception->getFile()}:{$exception->getLine()}",
            'trace' => $this->buildTrace($exception),
        ];

        $previous = $exception->getPrevious();

        if ($previous !== null) {
            $data['previous'] = $this->formatExceptionFragment($previous);
        }

        return $data;
    }

    /**
     * @return string[]
     */
    private function buildTrace(Throwable $exception): array
    {
        $frames = [];
        /** @var array{file: string|null, line: int} $frame */
        foreach ($exception->getTrace() as $frame) {
            if (isset($frame['file'])) {
                $frames[] = "{$frame['file']}:{$frame['line']}";
            }
        }

        return $frames;
    }
}
