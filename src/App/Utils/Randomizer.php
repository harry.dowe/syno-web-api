<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\App\Utils;

use Symfony\Component\DependencyInjection\Attribute\Exclude;

#[Exclude]
final readonly class Randomizer
{
    public static function getFloat(): float
    {
        return (mt_rand() / mt_getrandmax()) * time() * 1000;
    }
}
