<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\App\Log;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

final class LoggerConfiguratorCompilePass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        if (!$container->hasDefinition('monolog.logger_prototype')) {
            return;
        }

        if (!$container->hasDefinition('monolog.configurator')) {
            return;
        }

        $container
            ->getDefinition('monolog.logger_prototype')
            ->setConfigurator(new Reference('monolog.configurator'));
    }
}
