<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\App\Log;

use Monolog\Attribute\AsMonologProcessor;
use Monolog\Level;
use Monolog\Logger;
use Monolog\LogRecord;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

final readonly class HttpStatusCodeProcessor
{
    private Level $level;
    private Level $threshold;

    /**
     * @param int[] $httpCodes
     */
    public function __construct(
        #[Autowire(param: 'monolog.excluded_http_codes')]
        private array $httpCodes,
        int|string|Level $level = Level::Debug,
        int|string|Level $threshold = Level::Critical,
    ) {
        $this->level = Logger::toMonologLevel($level);
        $this->threshold = Logger::toMonologLevel($threshold);
    }

    /**
     * Mutates records containing an HTTP exception with a matching status code to a given level
     * provided that the log record's level is below the threshold.
     */
    #[AsMonologProcessor('request')]
    public function __invoke(LogRecord $record): LogRecord
    {
        return match (false) {
            $record->level->value < $this->threshold->value,
            isset($record->context['exception']),
            $record->context['exception'] instanceof HttpExceptionInterface,
            in_array($record->context['exception']->getStatusCode(), $this->httpCodes, true) => $record,
            default => $record->with(level: $this->level),
        };
    }
}
