<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\App\Log;

use Monolog\Handler\HandlerInterface;
use Monolog\Logger;
use Symfony\Component\DependencyInjection\Attribute\When;

use const PHP_SAPI;

#[When('development')]
final class LoggerConfigurator
{
    private ?HandlerInterface $handler = null;

    /**
     * @var string[]
     */
    private ?array $excludeChannels = null;

    /**
     * @param string[] $excludedChannels
     */
    public function __construct(
        HandlerInterface $handler,
        array $excludedChannels,
        ?bool $enable = null,
    ) {
        if ($enable ?? !in_array(PHP_SAPI, ['cli', 'phpdbg', 'embed'], true)) {
            $this->handler = $handler;
            $this->excludeChannels = $excludedChannels;
        }
    }

    public function __invoke(Logger $logger): void
    {
        if (!$this->handler) {
            return;
        }

        if ($this->permitsChannel($logger->getName())) {
            $logger->pushHandler($this->handler);
        }
    }

    private function permitsChannel(string $name): bool
    {
        return !in_array($name, $this->excludeChannels, true);
    }
}
