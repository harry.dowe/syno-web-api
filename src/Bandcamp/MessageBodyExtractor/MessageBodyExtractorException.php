<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp\MessageBodyExtractor;

use RuntimeException;

final class MessageBodyExtractorException extends RuntimeException
{
}
