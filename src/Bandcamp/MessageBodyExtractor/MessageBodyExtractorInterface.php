<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp\MessageBodyExtractor;

use Psr\Http\Message\UriInterface;

interface MessageBodyExtractorInterface
{
    /**
     * @throws MessageBodyExtractorException When the link can't be extracted
     */
    public function extractDownloadPageUri(string $email): UriInterface;
}
