<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp\MessageBodyExtractor;

use GuzzleHttp\Psr7\Uri;
use Psr\Http\Message\UriInterface;

final readonly class MessageBodyUriExtractor implements MessageBodyExtractorInterface
{
    private string $linkRegex;

    public function __construct(
        string $host = 'bandcamp.com',
    ) {
        $this->linkRegex = '#https?://' . preg_quote($host, '#') . '/download\?from=(?:receipt|email&id=\d+)&payment_id=\d+&sig=[a-z\d]+#i';
    }

    /**
     * Extracts the download page link from the email body.
     *
     * @throws MessageBodyExtractorException
     */
    public function extractDownloadPageUri(string $email): UriInterface
    {
        $matches = [];

        return match (preg_match($this->linkRegex, $email, $matches) === 1) {
            true => new Uri($matches[0]),
            default => throw new MessageBodyExtractorException('Unable to extract uri from email'),
        };
    }
}
