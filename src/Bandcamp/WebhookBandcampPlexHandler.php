<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp;

use GuzzleHttp\Exception\GuzzleException;
use HarryDowe\SynoWebApp\Bandcamp\Dom\BandcampDomCrawlerInterface;
use HarryDowe\SynoWebApp\Bandcamp\Filesystem\DownloadItemFilesystemInterface;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\Art\ArtDownloaderInterface;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\DownloadItemType;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\Music\MusicDownloaderInterface;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\Page\DownloadPageGatewayInterface;
use HarryDowe\SynoWebApp\Bandcamp\MessageBodyExtractor\MessageBodyExtractorInterface;
use Monolog\Attribute\WithMonologChannel;
use Psr\Log\LoggerInterface;

#[WithMonologChannel('bandcamp')]
final readonly class WebhookBandcampPlexHandler implements WebhookBandcampPlexHandlerInterface
{
    public function __construct(
        private MessageBodyExtractorInterface $messageBodyExtractor,
        private DownloadPageGatewayInterface $downloadPageGateway,
        private BandcampDomCrawlerInterface $crawler,
        private MusicDownloaderInterface $musicDownloader,
        private ArtDownloaderInterface $artDownloader,
        private DownloadItemFilesystemInterface $filesystem,
        private LoggerInterface $logger,
    ) {
    }

    /**
     * Attempts to read the email and crawl Bandcamp to download the tunes
     * and either extract the zip or put the track and cover art into Plex's library.
     *
     * {@inheritdoc}
     */
    public function processEmail(string $email): array
    {
        $downloadPageUri = $this->messageBodyExtractor->extractDownloadPageUri($email);

        $html = $this->downloadPageGateway->getDownloadPage($downloadPageUri);

        $downloadItems = $this->crawler->getDownloadItems($html);

        foreach ($downloadItems as $downloadItem) {
            $this->musicDownloader->downloadMusic($downloadItem);

            if ($downloadItem->type === DownloadItemType::Album) {
                $this->filesystem->extractZip($downloadItem);

                continue;
            }

            try {
                $this->artDownloader->downloadArt($downloadItem);
            } catch (GuzzleException $exception) {
                $this->logger->error('Failed to download art', [
                    'exception' => $exception,
                ]);
            }

            $this->filesystem->moveTrack($downloadItem);
        }

        return $downloadItems;
    }
}
