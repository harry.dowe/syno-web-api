<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp\Events;

use HarryDowe\SynoWebApp\Bandcamp\Filesystem\DownloadItemFilesystemInterface;
use HarryDowe\SynoWebApp\Bandcamp\Filesystem\FilesystemException;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\DownloadItemType;
use Monolog\Attribute\WithMonologChannel;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;

#[WithMonologChannel('bandcamp')]
final readonly class FilesystemCleanupEmailCompletedEventListener
{
    public function __construct(
        private DownloadItemFilesystemInterface $filesystem,
        private LoggerInterface $logger,
    ) {
    }

    #[AsEventListener]
    public function onEmailCompleted(EmailHandlerCompletedEvent $event): void
    {
        foreach ($event->downloadItems as $downloadItem) {
            if ($downloadItem->type !== DownloadItemType::Album) {
                continue;
            }

            try {
                $this->filesystem->deleteZip($downloadItem);
            } catch (FilesystemException $exception) {
                $this->logger->error('Failed to delete zip', ['exception' => $exception]);
            }
        }
    }
}
