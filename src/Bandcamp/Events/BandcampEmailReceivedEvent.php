<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp\Events;

use DateTimeImmutable;
use DateTimeInterface;

final readonly class BandcampEmailReceivedEvent
{
    public function __construct(
        public string $emailBody,
        public string $messageId,
        public DateTimeInterface $receivedAt = new DateTimeImmutable(),
    ) {
    }
}
