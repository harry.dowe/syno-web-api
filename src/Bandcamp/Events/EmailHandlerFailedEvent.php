<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp\Events;

use DateTimeImmutable;

final readonly class EmailHandlerFailedEvent
{
    public function __construct(
        public string $messageId,
        public DateTimeImmutable $completedAt = new DateTimeImmutable(),
    ) {
    }
}
