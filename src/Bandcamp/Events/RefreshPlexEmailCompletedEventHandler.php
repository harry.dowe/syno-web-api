<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp\Events;

use GuzzleHttp\Exception\GuzzleException;
use HarryDowe\SynoWebApp\Plex\Http\Client\PlexGatewayInterface;
use HarryDowe\SynoWebApp\Plex\PlexLibraryId;
use Monolog\Attribute\WithMonologChannel;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;

#[WithMonologChannel('bandcamp')]
final readonly class RefreshPlexEmailCompletedEventHandler
{
    public function __construct(
        private PlexGatewayInterface $plexGateway,
        private LoggerInterface $logger,
    ) {
    }

    #[AsEventListener(event: EmailHandlerCompletedEvent::class)]
    public function onEmailHandlerCompleted(): void
    {
        try {
            $this->plexGateway->refreshLibrary(PlexLibraryId::Music);

            $this->logger->info('Refreshed plex library');
        } catch (GuzzleException $exception) {
            $this->logger->error('Error found while refreshing plex library', [
                'exception' => $exception,
            ]);
        }
    }
}
