<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp\Events;

use DateTimeImmutable;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\DownloadItem;

final readonly class EmailHandlerCompletedEvent
{
    /**
     * @param non-empty-list<DownloadItem> $downloadItems
     */
    public function __construct(
        public string $messageId,
        public array $downloadItems,
        public DateTimeImmutable $completedAt = new DateTimeImmutable(),
    ) {
    }
}
