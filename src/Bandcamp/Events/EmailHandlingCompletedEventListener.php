<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp\Events;

use Doctrine\DBAL\Exception as DbalException;
use HarryDowe\SynoWebApp\Bandcamp\Repository\BandcampEmailRepositoryInterface;
use HarryDowe\SynoWebApp\Bandcamp\Repository\BandcampEmailStatus;
use Monolog\Attribute\WithMonologChannel;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;

#[WithMonologChannel('bandcamp')]
final readonly class EmailHandlingCompletedEventListener
{
    public function __construct(
        private BandcampEmailRepositoryInterface $db,
        private LoggerInterface $logger,
    ) {
    }

    #[AsEventListener]
    public function onEmailHandlerCompleted(EmailHandlerCompletedEvent $event): void
    {
        $this->handleEvent($event, BandcampEmailStatus::Completed);
    }

    #[AsEventListener]
    public function onEmailHandlerFailed(EmailHandlerFailedEvent $event): void
    {
        $this->handleEvent($event, BandcampEmailStatus::Failed);
    }

    public function handleEvent(
        EmailHandlerCompletedEvent|EmailHandlerFailedEvent $event,
        BandcampEmailStatus $status,
    ): void {
        $this->logger->info('Email process completed', [
            'status' => $status,
            'messageId' => $event->messageId,
        ]);

        try {
            $this->db->setEmailStatus(
                $status,
                $event->messageId,
                $event->completedAt,
            );
        } catch (DbalException $exception) {
            $this->logger->error("Failed to set email as {$status->name}", [
                'messageId' => $event->messageId,
                'exception' => $exception,
            ]);
        }
    }
}
