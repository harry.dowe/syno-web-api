<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp\Events;

use HarryDowe\SynoWebApp\Bandcamp\Repository\BandcampEmailRepositoryInterface;
use HarryDowe\SynoWebApp\Bandcamp\WebhookBandcampPlexHandlerInterface;
use Monolog\Attribute\WithMonologChannel;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Throwable;

#[WithMonologChannel('bandcamp')]
final readonly class BandcampPlexWebhookListener
{
    public function __construct(
        private WebhookBandcampPlexHandlerInterface $handler,
        private BandcampEmailRepositoryInterface $db,
        private EventDispatcherInterface $eventDispatcher,
        private LoggerInterface $logger,
    ) {
    }

    #[AsEventListener]
    public function onEmailReceived(BandcampEmailReceivedEvent $event): void
    {
        if ($this->db->emailExists($event->messageId)) {
            $this->logger->warning('Email already being processed', [
                'messageId' => $event->messageId,
            ]);

            return;
        }

        $this->db->createEmail($event->messageId, $event->receivedAt);

        try {
            $downloadItems = $this->handler->processEmail($event->emailBody);
        } catch (Throwable $exception) {
            $this->eventDispatcher->dispatch(new EmailHandlerFailedEvent($event->messageId));

            throw $exception;
        }

        $this->eventDispatcher->dispatch(new EmailHandlerCompletedEvent(
            $event->messageId,
            $downloadItems,
        ));
    }
}
