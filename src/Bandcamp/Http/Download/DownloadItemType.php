<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp\Http\Download;

enum DownloadItemType: string
{
    case Track = 'track';
    case Album = 'album';

    public function extension(): string
    {
        return match ($this) {
            self::Track => 'flac',
            self::Album => 'zip',
        };
    }
}
