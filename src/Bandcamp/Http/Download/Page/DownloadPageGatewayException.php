<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp\Http\Download\Page;

use RuntimeException;

final class DownloadPageGatewayException extends RuntimeException
{
}
