<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp\Http\Download\Page;

use Psr\Http\Message\UriInterface;

interface DownloadPageGatewayInterface
{
    /**
     * @throws DownloadPageGatewayException when the page cannot be downloaded
     */
    public function getDownloadPage(UriInterface $uri): string;
}
