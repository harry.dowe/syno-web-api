<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp\Http\Download\Page;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\UriInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpFoundation\Request;

final readonly class DownloadPageGateway implements DownloadPageGatewayInterface
{
    public function __construct(
        #[Autowire(service: 'app.http.client.bandcamp')]
        private ClientInterface $http,
    ) {
    }

    /**
     * @throws DownloadPageGatewayException when the page cannot be downloaded
     */
    public function getDownloadPage(UriInterface $uri): string
    {
        try {
            $response = $this->http->request(Request::METHOD_GET, $uri->withScheme('https'));
        } catch (GuzzleException $exception) {
            throw new DownloadPageGatewayException('Error fetching download page', previous: $exception);
        }

        return (string) $response->getBody();
    }
}
