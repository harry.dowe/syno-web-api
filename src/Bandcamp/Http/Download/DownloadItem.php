<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp\Http\Download;

use DateTimeInterface;
use Psr\Http\Message\UriInterface;

final readonly class DownloadItem
{
    public function __construct(
        public int $itemId,
        public UriInterface $uri,
        public string $artist,
        public string $title,
        public int $artId,
        public DownloadItemType $type,
        public ?DateTimeInterface $releaseDate,
    ) {
    }

    public function getFilename(): string
    {
        return "{$this->itemId}.{$this->extension()}";
    }

    public function extension(): string
    {
        return $this->type->extension();
    }

    public function getArtFilename(): string
    {
        return "{$this->itemId}.jpg";
    }
}
