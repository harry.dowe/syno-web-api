<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp\Http\Download\Art;

use GuzzleHttp\Exception\GuzzleException;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\DownloadItem;

interface ArtDownloaderInterface
{
    /**
     * @throws GuzzleException
     */
    public function downloadArt(DownloadItem $downloadItem): void;
}
