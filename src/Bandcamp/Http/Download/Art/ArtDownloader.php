<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp\Http\Download\Art;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\DownloadItem;
use League\Flysystem\PathPrefixer;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpFoundation\Request;

final readonly class ArtDownloader implements ArtDownloaderInterface
{
    private PathPrefixer $prefixer;

    public function __construct(
        #[Autowire(service: 'app.http.client.bandcamp_bcbits')]
        private ClientInterface $http,
        #[Autowire(param: 'app.bandcamp.filesystem.storage.location')]
        string $storageLocation,
    ) {
        $this->prefixer = new PathPrefixer($storageLocation);
    }

    /**
     * @throws GuzzleException
     */
    public function downloadArt(DownloadItem $downloadItem): void
    {
        $filepath = $this->prefixer->prefixPath($downloadItem->getArtFilename());

        $this->http->request(Request::METHOD_GET, "img/a{$downloadItem->artId}_10.jpg", [
            RequestOptions::SINK => $filepath,
        ]);
    }
}
