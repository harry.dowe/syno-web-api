<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp\Http\Download\Music;

use HarryDowe\SynoWebApp\Bandcamp\Http\Download\DownloadItem;

interface MusicDownloaderInterface
{
    /**
     * @throws MusicDownloaderException
     */
    public function downloadMusic(DownloadItem $downloadItem): void;
}
