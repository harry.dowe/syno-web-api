<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp\Http\Download\Music;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use HarryDowe\SynoWebApp\App\Utils\Randomizer;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\DownloadItem;
use JsonException;
use League\Flysystem\PathPrefixer;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpFoundation\Request;

use const JSON_THROW_ON_ERROR;

final readonly class MusicDownloader implements MusicDownloaderInterface
{
    private PathPrefixer $prefixer;

    public function __construct(
        #[Autowire(service: 'app.http.client.bandcamp_popplers5')]
        private ClientInterface $http,
        #[Autowire(param: 'app.bandcamp.filesystem.storage.location')]
        string $storageLocation,
    ) {
        $this->prefixer = new PathPrefixer($storageLocation);
    }

    /**
     * Performs the /statdownload/{type} API call and downloads the music to application storage.
     *
     * @throws MusicDownloaderException
     */
    public function downloadMusic(DownloadItem $downloadItem): void
    {
        try {
            $response = $this->doStatDownloadApiCall($downloadItem);
        } catch (GuzzleException $exception) {
            throw new MusicDownloaderException('API call to /statdownload/* failed', previous: $exception);
        }

        try {
            /**
             * @var array{
             *     result: "ok",
             *     download_url: string,
             * }|array{
             *     result: "err",
             *     retry_url: string,
             *     errortype: string,
             *     date: string,
             *     url: string,
             *     host: string,
             * } $body
             */
            $body = json_decode((string) $response->getBody(), true, flags: JSON_THROW_ON_ERROR);
        } catch (JsonException $exception) {
            throw new MusicDownloaderException('Unable to parse API call response', previous: $exception);
        }

        if (!isset($body['result']) || $body['result'] === 'err') {
            throw new MusicDownloaderException("API call to /statdownload/* failed: {$body['errortype']}");
        }

        try {
            $this->http->request(Request::METHOD_GET, $body['download_url'], [
                RequestOptions::SINK => $this->prefixer->prefixPath($downloadItem->getFilename()),
            ]);
        } catch (GuzzleException $exception) {
            throw new MusicDownloaderException('Music download failed', previous: $exception);
        }
    }

    /**
     * @throws GuzzleException
     */
    private function doStatDownloadApiCall(DownloadItem $downloadItem): ResponseInterface
    {
        $params = [];
        parse_str($downloadItem->uri->getQuery(), $params);

        $path = str_replace('/download', 'statdownload', $downloadItem->uri->getPath());

        return $this->http->request(Request::METHOD_GET, $path, [
            RequestOptions::QUERY => array_merge($params, [
                '.vrs' => 1,
                '.rand' => number_format(Randomizer::getFloat(), thousands_separator: ''),
            ]),
            RequestOptions::HEADERS => [
                'Accept' => 'application/json',
            ],
        ]);
    }
}
