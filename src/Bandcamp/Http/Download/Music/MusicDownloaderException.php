<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp\Http\Download\Music;

use RuntimeException;

final class MusicDownloaderException extends RuntimeException
{
}
