<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp\Http\Controller;

use HarryDowe\SynoWebApp\App\Http\Auth\Basic\BasicAuthenticatedController;
use HarryDowe\SynoWebApp\App\Http\Auth\Basic\WithBasicAuthentication;
use HarryDowe\SynoWebApp\Bandcamp\Events\BandcampEmailReceivedEvent;
use Monolog\Attribute\WithMonologChannel;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Attribute\Route;
use Throwable;

#[WithMonologChannel('bandcamp')]
final class BandcampPlexWebhookController extends AbstractController implements BasicAuthenticatedController
{
    public function __construct(
        private readonly EventDispatcherInterface $eventDispatcher,
        private readonly LoggerInterface $logger,
    ) {
    }

    /**
     * The endpoint called by make.com. It receives a json object
     * containing a base64 encoded email body and the email ID.
     *
     * @throws BadRequestException When request body validation fails
     */
    #[Route(path: '/bandcamp/make-webhook', methods: [Request::METHOD_POST])]
    #[WithBasicAuthentication('app.bandcamp.webhook_http_user', 'app.bandcamp.webhook_http_password')]
    public function makeWebhook(Request $request): Response
    {
        $json = $this->getJson($request);

        $messageId = $json->get('messageId');

        $this->logger->info('Received message {messageId}', ['messageId' => $messageId]);

        $success = $this->dispatchEvent($json->get('body'), $messageId);

        return new JsonResponse([
            'success' => $success,
        ]);
    }

    /**
     * @throws BadRequestHttpException
     */
    private function getJson(Request $request): InputBag
    {
        $json = new InputBag($request->toArray());

        $body = $json->get('body');

        if (!is_string($body)) {
            throw new BadRequestHttpException('Email body is not a string');
        }

        $body = base64_decode($body, true);

        if ($body === false) {
            throw new BadRequestHttpException('Invalid base64 encoded email body');
        }

        if (!$json->has('messageId')) {
            throw new BadRequestHttpException('messageId not set');
        }

        $messageId = $json->get('messageId');

        if (!is_string($messageId)) {
            throw new BadRequestHttpException('messageId is not a string');
        }

        if (strlen($messageId) > 255) {
            throw new BadRequestHttpException('messageId is longer than 255 characters');
        }

        $json->set('body', $body);

        return $json;
    }

    /**
     * @param non-empty-string $body
     * @param non-empty-string $messageId
     */
    private function dispatchEvent(string $body, string $messageId): bool
    {
        try {
            $this->eventDispatcher->dispatch(new BandcampEmailReceivedEvent($body, $messageId));
        } catch (Throwable $exception) {
            $this->logger->critical('Error while handling webhook event', ['exception' => $exception]);

            return false;
        }

        return true;
    }
}
