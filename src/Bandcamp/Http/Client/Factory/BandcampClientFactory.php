<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp\Http\Client\Factory;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;

final readonly class BandcampClientFactory
{
    public function __invoke(?string $baseUri = null): ClientInterface
    {
        return new Client([
            'base_uri' => $baseUri,
            RequestOptions::VERSION => '2.0',
            RequestOptions::HEADERS => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64)',
            ],
        ]);
    }
}
