<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp;

use HarryDowe\SynoWebApp\Bandcamp\Dom\BandcampDomCrawlerException;
use HarryDowe\SynoWebApp\Bandcamp\Filesystem\FilesystemException;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\DownloadItem;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\Music\MusicDownloaderException;
use HarryDowe\SynoWebApp\Bandcamp\MessageBodyExtractor\MessageBodyExtractorException;

interface WebhookBandcampPlexHandlerInterface
{
    /**
     * @return DownloadItem[]
     *
     * @throws MessageBodyExtractorException
     * @throws BandcampDomCrawlerException
     * @throws MusicDownloaderException
     * @throws FilesystemException
     */
    public function processEmail(string $email): array;
}
