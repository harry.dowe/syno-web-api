<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp\Repository;

enum BandcampEmailStatus
{
    case Completed;
    case Failed;

    public function table(): string
    {
        return match ($this) {
            self::Completed => 'completed_at',
            self::Failed => 'failed_at',
        };
    }
}
