<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp\Repository;

use DateTimeInterface;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception as DbalException;

final readonly class BandcampEmailRepository implements BandcampEmailRepositoryInterface
{
    public function __construct(
        private Connection $db,
    ) {
    }

    /**
     * @throws DbalException
     */
    public function createEmail(
        string $messageId,
        DateTimeInterface $createdAt,
    ): void {
        $qb = $this->db->createQueryBuilder();

        $qb->insert('bandcamp_emails')
            ->values([
                'message_id' => ':messageId',
                'created_at' => ':created_at',
            ])
            ->setParameters([
                'messageId' => $messageId,
                'created_at' => $createdAt->format('Y-m-d H:i:s'),
            ])
            ->executeStatement();
    }

    /**
     * @throws DbalException
     */
    public function setEmailStatus(
        BandcampEmailStatus $status,
        string $messageId,
        DateTimeInterface $timestamp,
    ): void {
        $qb = $this->db->createQueryBuilder();

        $qb->update('bandcamp_emails')
            ->set($status->table(), ':timestamp')
            ->where('message_id = :message_id')
            ->setParameter('timestamp', $timestamp->format('Y-m-d H:i:s'))
            ->setParameter('message_id', $messageId)
            ->executeStatement();
    }

    /**
     * @throws DbalException
     */
    public function emailExists(string $messageId): bool
    {
        $qb = $this->db->createQueryBuilder();

        $result = $qb->select('count(*) as count')
            ->from('bandcamp_emails')
            ->where('message_id = :message_id')
            ->setParameter('message_id', $messageId)
            ->executeQuery()
            ->fetchFirstColumn();

        return $result[0] > 0;
    }
}
