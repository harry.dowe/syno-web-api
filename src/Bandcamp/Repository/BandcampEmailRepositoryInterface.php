<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp\Repository;

use DateTimeInterface;

interface BandcampEmailRepositoryInterface
{
    /**
     * @throws \Doctrine\DBAL\Exception
     */
    public function createEmail(string $messageId, DateTimeInterface $createdAt): void;

    /**
     * @throws \Doctrine\DBAL\Exception
     */
    public function setEmailStatus(BandcampEmailStatus $status, string $messageId, DateTimeInterface $timestamp): void;

    /**
     * @throws \Doctrine\DBAL\Exception
     */
    public function emailExists(string $messageId): bool;
}
