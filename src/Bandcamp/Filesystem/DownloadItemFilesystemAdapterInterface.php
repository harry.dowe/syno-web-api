<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp\Filesystem;

interface DownloadItemFilesystemAdapterInterface
{
    /**
     * @throws FilesystemException
     */
    public function extract(string $source, string $destination): void;

    /**
     * @throws FilesystemException
     */
    public function move(string $source, string $destination): void;

    /**
     * @throws FilesystemException
     */
    public function delete(string $location): void;
}
