<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp\Filesystem;

use HarryDowe\SynoWebApp\Bandcamp\Http\Download\DownloadItem;
use InvalidArgumentException;

interface DownloadItemFilesystemInterface
{
    /**
     * @throws FilesystemException
     * @throws InvalidArgumentException
     */
    public function extractZip(DownloadItem $downloadItem): void;

    /**
     * @throws FilesystemException
     * @throws InvalidArgumentException
     */
    public function deleteZip(DownloadItem $downloadItem): void;

    /**
     * @throws FilesystemException
     * @throws InvalidArgumentException
     */
    public function moveTrack(DownloadItem $downloadItem): void;
}
