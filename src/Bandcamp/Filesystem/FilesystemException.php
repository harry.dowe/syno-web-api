<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp\Filesystem;

use RuntimeException;

final class FilesystemException extends RuntimeException
{
}
