<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp\Filesystem;

use HarryDowe\SynoWebApp\Bandcamp\Http\Download\DownloadItem;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\DownloadItemType;
use InvalidArgumentException;
use Monolog\Attribute\WithMonologChannel;
use Psr\Log\LoggerInterface;

#[WithMonologChannel('bandcamp')]
final readonly class DownloadItemFilesystem implements DownloadItemFilesystemInterface
{
    public function __construct(
        private DownloadItemFilesystemAdapterInterface $filesystemAdapter,
        private LoggerInterface $logger,
    ) {
    }

    /**
     * Extracts the zip archive from Symfony's storage to Plex's media library.
     *
     * @throws FilesystemException
     * @throws InvalidArgumentException
     */
    public function extractZip(DownloadItem $downloadItem): void
    {
        if ($downloadItem->type !== DownloadItemType::Album) {
            throw new InvalidArgumentException('$downloadItem->type must be Album');
        }

        $this->filesystemAdapter->extract(
            source: $downloadItem->getFilename(),
            destination: $this->getTitleDirectory($downloadItem),
        );
    }

    /**
     * Deletes the downloaded zip file.
     *
     * @throws FilesystemException
     * @throws InvalidArgumentException
     */
    public function deleteZip(DownloadItem $downloadItem): void
    {
        if ($downloadItem->type !== DownloadItemType::Album) {
            throw new InvalidArgumentException('$downloadItem->type must be Album');
        }

        $this->filesystemAdapter->delete($downloadItem->getFilename());
    }

    /**
     * Moves the track and art from Symfony's storage to Plex's media library.
     *
     * @throws FilesystemException
     * @throws InvalidArgumentException
     */
    public function moveTrack(DownloadItem $downloadItem): void
    {
        if ($downloadItem->type !== DownloadItemType::Track) {
            throw new InvalidArgumentException('$downloadItem->type must be Track');
        }

        try {
            $this->filesystemAdapter->move(
                source: $downloadItem->getArtFilename(),
                destination: "{$this->getTitleDirectory($downloadItem)}/cover.jpg",
            );
        } catch (FilesystemException $exception) {
            $this->logger->error('Failed to move art file into plex media library', [
                'exception' => $exception,
            ]);
        }

        $this->filesystemAdapter->move(
            source: $downloadItem->getFilename(),
            destination: $this->getTitleFilePath($downloadItem),
        );
    }

    /**
     * Gets the directory path relative to the plex root
     * eg 'Bonobo/(2017) Migration' or 'Bonobo/Migration' if the date is not available.
     */
    private function getTitleDirectory(DownloadItem $downloadItem): string
    {
        return match ($downloadItem->releaseDate === null) {
            true => "{$downloadItem->artist}/{$downloadItem->title}",
            default => "{$downloadItem->artist}/({$downloadItem->releaseDate->format('Y')}) {$downloadItem->title}",
        };
    }

    /**
     * Gets the file path, eg 'LORN/ENTROPYYY/LORN - ENTROPYYY.flac'.
     */
    private function getTitleFilePath(DownloadItem $downloadItem): string
    {
        return sprintf(
            '%s/%s - %s.%s',
            $this->getTitleDirectory($downloadItem),
            $downloadItem->artist,
            $downloadItem->title,
            $downloadItem->extension(),
        );
    }
}
