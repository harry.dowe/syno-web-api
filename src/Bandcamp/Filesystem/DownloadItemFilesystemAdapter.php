<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp\Filesystem;

use League\Flysystem\FilesystemException as FlysystemException;
use League\Flysystem\FilesystemWriter;
use League\Flysystem\PathPrefixer;
use PhpZip\Exception\ZipException;
use PhpZip\ZipFile;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

final readonly class DownloadItemFilesystemAdapter implements DownloadItemFilesystemAdapterInterface
{
    private PathPrefixer $sourcePrefixer;
    private PathPrefixer $destinationPrefixer;

    public function __construct(
        private ZipFile $zip,
        #[Autowire(param: 'app.bandcamp.filesystem.storage.location')]
        string $sourceLocation,
        #[Autowire(param: 'app.bandcamp.filesystem.plex.location')]
        string $destinationLocation,
        #[Autowire(service: 'app.filesystem.disk.root')]
        private FilesystemWriter $filesystem,
    ) {
        $this->sourcePrefixer = new PathPrefixer($sourceLocation);
        $this->destinationPrefixer = new PathPrefixer($destinationLocation);
    }

    /**
     * @throws FilesystemException
     */
    public function extract(string $source, string $destination): void
    {
        try {
            $this->filesystem->createDirectory(
                location: $this->destinationPrefixer->prefixDirectoryPath($destination),
            );
        } catch (FlysystemException $exception) {
            throw new FilesystemException('Unable to create destination directory', previous: $exception);
        }

        try {
            $this->zip
                ->openFile($this->sourcePrefixer->prefixPath($source))
                ->extractTo($this->destinationPrefixer->prefixDirectoryPath($destination));
        } catch (ZipException $exception) {
            throw new FilesystemException('Unable to extract zip archive', previous: $exception);
        } finally {
            $this->zip->close();
        }
    }

    /**
     * @throws FilesystemException
     */
    public function move(string $source, string $destination): void
    {
        try {
            $this->filesystem->move(
                source: $this->sourcePrefixer->prefixPath($source),
                destination: $this->destinationPrefixer->prefixPath($destination),
            );
        } catch (FlysystemException $exception) {
            throw new FilesystemException('Unable to move file', previous: $exception);
        }
    }

    /**
     * @throws FilesystemException
     */
    public function delete(string $location): void
    {
        try {
            $this->filesystem->delete($location);
        } catch (FlysystemException $exception) {
            throw new FilesystemException('Unable to delete file', previous: $exception);
        }
    }
}
