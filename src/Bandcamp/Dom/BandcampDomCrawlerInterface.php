<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp\Dom;

use HarryDowe\SynoWebApp\Bandcamp\Http\Download\DownloadItem;

interface BandcampDomCrawlerInterface
{
    /**
     * @return non-empty-list<DownloadItem>
     *
     * @throws BandcampDomCrawlerException When the web page cannot be crawled correctly
     */
    public function getDownloadItems(string $html): array;
}
