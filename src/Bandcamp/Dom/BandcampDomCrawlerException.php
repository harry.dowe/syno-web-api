<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp\Dom;

use RuntimeException;

final class BandcampDomCrawlerException extends RuntimeException
{
}
