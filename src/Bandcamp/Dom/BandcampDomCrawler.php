<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Bandcamp\Dom;

use DateTimeImmutable;
use DateTimeInterface;
use GuzzleHttp\Psr7\Uri;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\DownloadItem;
use HarryDowe\SynoWebApp\Bandcamp\Http\Download\DownloadItemType;
use InvalidArgumentException;
use JsonException;
use Psr\Http\Message\UriInterface;
use Symfony\Component\DomCrawler\Crawler;
use ValueError;
use const JSON_THROW_ON_ERROR;

/**
 * @psalm-type BandcampDownloadPageDataBlobPayload = array{
 *     download_items: list<
 *         array{
 *             item_id: int,
 *             artist?: string,
 *             band_name: string,
 *             title: string,
 *             type: string,
 *             art_id: int,
 *             release_date?: string,
 *             downloads: array{
 *                 flac?: array{
 *                     url: string,
 *                 },
 *                 vorbis?: array{
 *                     url: string,
 *                 },
 *                 "mp3-320"?: array{
 *                     url: string,
 *                 },
 *             },
 *         },
 *     >,
 * }
 */
final readonly class BandcampDomCrawler implements BandcampDomCrawlerInterface
{
    public function __construct(
        private Crawler $crawler,
    ) {
    }

    /**
     * {@inheritdoc}
     */
    public function getDownloadItems(string $html): array
    {
        $this->crawler->clear();

        $this->crawler->addHtmlContent($html);

        $node = $this->crawler->filterXPath("//html/body/div[@id='pagedata']");

        try {
            $result = $node->attr('data-blob');
        } catch (InvalidArgumentException $exception) {
            throw new BandcampDomCrawlerException('Unable to extract data-blob attribute', previous: $exception);
        }

        if ($result === null) {
            throw new BandcampDomCrawlerException('Unable to extract data-blob attribute');
        }

        $json = $this->decodeAttribute($result);

        $downloadItems = [];
        foreach ($json['download_items'] ?? $json['digital_items'] ?? [] as $downloadItem) {
            try {
                $type = DownloadItemType::from($downloadItem['type']);
            } catch (ValueError $exception) {
                throw new BandcampDomCrawlerException('Invalid download item type', previous: $exception);
            }

            $downloadItems[] = new DownloadItem(
                itemId: $downloadItem['item_id'],
                uri: $this->getUri($downloadItem),
                artist: $downloadItem['artist'] ?? $downloadItem['band_name'],
                title: $downloadItem['title'],
                artId: $downloadItem['art_id'],
                type: $type,
                releaseDate: $this->getReleaseDate($downloadItem),
            );
        }

        if (empty($downloadItems)) {
            throw new BandcampDomCrawlerException('Unable to extract download items');
        }

        return $downloadItems;
    }

    /**
     * @psalm-return BandcampDownloadPageDataBlobPayload
     *
     * @throws BandcampDomCrawlerException
     */
    private function decodeAttribute(string $result): array
    {
        try {
            return json_decode($result, true, flags: JSON_THROW_ON_ERROR);
        } catch (JsonException $exception) {
            throw new BandcampDomCrawlerException('Unable to decode JSON string', previous: $exception);
        }
    }

    /**
     * @param array{
     *     release_date?: string,
     *     ...
     * } $downloadItem
     */
    private function getReleaseDate(array $downloadItem): ?DateTimeInterface
    {
        if (isset($downloadItem['release_date'])) {
            return DateTimeImmutable::createFromFormat('d M Y H:i:s T', $downloadItem['release_date']) ?: null;
        }

        return null;
    }

    /**
     * @param array{downloads?: array{flac?: array{url?: string}}, ...} $downloadItem
     *
     * @throws BandcampDomCrawlerException
     */
    private function getUri(array $downloadItem): UriInterface
    {
        $uri = $downloadItem['downloads']['flac']['url'] ?? null;

        return match (true) {
            $uri !== null => new Uri($uri),
            default => throw new BandcampDomCrawlerException('Unable to find download link'),
        };
    }
}
