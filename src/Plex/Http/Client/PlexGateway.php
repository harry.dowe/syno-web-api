<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Plex\Http\Client;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use HarryDowe\SynoWebApp\Plex\PlexLibraryId;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpFoundation\Request;

final readonly class PlexGateway implements PlexGatewayInterface
{
    public function __construct(
        #[Autowire(service: 'app.http.client.plex')]
        private ClientInterface $http,
    ) {
    }

    /**
     * @throws GuzzleException
     */
    public function refreshLibrary(PlexLibraryId $id): void
    {
        $this->http->request(Request::METHOD_GET, "library/sections/{$id->value}/refresh");
    }
}
