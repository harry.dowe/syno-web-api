<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Plex\Http\Client;

use GuzzleHttp\Exception\GuzzleException;
use HarryDowe\SynoWebApp\Plex\PlexLibraryId;

interface PlexGatewayInterface
{
    /**
     * @throws GuzzleException
     */
    public function refreshLibrary(PlexLibraryId $id): void;
}
