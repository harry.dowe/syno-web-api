<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Plex\Http\Client;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;
use SensitiveParameter;

final readonly class PlexClientFactory
{
    public function __invoke(
        string $baseUri,
        #[SensitiveParameter]
        string $token,
    ): ClientInterface {
        return new Client([
            'base_uri' => $baseUri,
            RequestOptions::VERSION => '2.0',
            RequestOptions::HEADERS => [
                'X-Plex-Token' => $token,
            ],
        ]);
    }
}
