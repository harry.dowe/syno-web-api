<?php

declare(strict_types=1);

namespace HarryDowe\SynoWebApp\Plex;

enum PlexLibraryId: int
{
    case Music = 2;
    case Films = 3;
    case Programmes = 4;
}
