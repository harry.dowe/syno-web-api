<?php

declare(strict_types=1);

namespace HarryDowe\DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240416170536 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Creating `bandcamp_emails` table';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(<<<'SQL'
            create table bandcamp_emails
            (
                id             int auto_increment primary key,
                message_id varchar(255) not null,
                created_at     datetime     not null,
                completed_at   datetime     null,
                failed_at      datetime     null,
                constraint bandcamp_emails_gmail_email_id_uindex
                    unique (message_id)
            );
            SQL,
        );
    }

    public function down(Schema $schema): void
    {
    }
}
