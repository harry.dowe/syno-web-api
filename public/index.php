<?php

declare(strict_types=1);

use HarryDowe\SynoWebApp\Kernel;
use Symfony\Component\HttpKernel\KernelInterface;

require_once dirname(__DIR__) . '/vendor/autoload_runtime.php';

return static function (array $context): KernelInterface {
    return new Kernel($context['APP_ENV'], (bool) $context['APP_DEBUG']);
};
