FROM php:8.2-fpm-alpine3.19

# Install dependencies
RUN apk add linux-headers icu-dev $PHPIZE_DEPS \
    && pecl install xdebug \
    && docker-php-ext-install pdo_mysql intl sockets \
    && docker-php-ext-enable xdebug intl pdo_mysql sockets

# Configure symfony
RUN mkdir -p /var/www/html/var/cache \
    && mkdir -p /var/www/html/var/storage/bandcamp \
    && mkdir -p /var/www/html/var/storage/plex \
    && chown -R www-data:www-data /var/www/html/var \
    && chmod 1777 /var/www/html/var

USER www-data

COPY docker/xdebug.ini /usr/local/etc/php/conf.d/xdebug.ini
